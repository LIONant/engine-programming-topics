
namespace engprog::linear_containers
{
    std::string GetLastErrorAsString()
    {
        //Get the error message, if any.
        DWORD errorMessageID = ::GetLastError();
        if (errorMessageID == 0)
            return std::string(); //No error message has been recorded

        LPSTR messageBuffer = nullptr;
        size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

        std::string message(messageBuffer, size);

        //Free the buffer.
        LocalFree(messageBuffer);

        return message;
    }
    template< typename T_TYPE, std::size_t T_MAX_ENTRIES_V >
    class pool_v2
    {
    public:

        constexpr static std::size_t    max_entries_v       = T_MAX_ENTRIES_V;
        using                           entry               = T_TYPE;
        using                           self                = pool_v2;
        using                           iterator            = entry*;

    public:

        constexpr   auto begin()                        const   noexcept { return m_Count?m_pEntries:nullptr;           }
                    auto begin()                                noexcept { return m_Count?m_pEntries : nullptr;         }
        constexpr   auto end()                          const   noexcept { return m_Count?&m_pEntries[m_Count]:nullptr; }
        constexpr   auto size()                         const   noexcept { return m_Count; }

        pool_v2()
        {
            if (s_PageSize == 0)
            {
                SYSTEM_INFO sSysInfo;
                GetSystemInfo(&sSysInfo);
                s_PageSize     = sSysInfo.dwPageSize;
                s_PageSizePow2 = xcore::bits::Log2Int(s_PageSize);
            }

            std::size_t PageCount  = (max_entries_v * sizeof(entry)) / s_PageSize;
            PageCount              = ((PageCount * s_PageSize) < (max_entries_v * sizeof(entry))) ? PageCount +1 : PageCount;
            xassert( (PageCount * s_PageSize) >= (max_entries_v * sizeof(entry)));

            m_pEntries = reinterpret_cast<entry*>(VirtualAlloc(nullptr, PageCount * s_PageSize, MEM_RESERVE, PAGE_NOACCESS));
        }

        ~pool_v2(void) noexcept
        {
            if constexpr (std::is_destructible_v<entry>)
            {
                clear();
            }

            // TODO: add the free
            if( m_pEntries ) VirtualFree( m_pEntries, 0, MEM_RELEASE );
        }

        template< typename...T_ARGS >
        std::size_t append(T_ARGS&&... Args) noexcept
        {
            const auto ByteOffset = m_Count * sizeof(entry);
            const auto k          = (ByteOffset + sizeof(entry)) >> s_PageSizePow2;
            if (m_Count == 0 || k != (ByteOffset >> s_PageSizePow2))
            {
                auto pRaw = &reinterpret_cast<std::byte*>(m_pEntries)[k * s_PageSize];
                auto p    = VirtualAlloc(pRaw, s_PageSize, MEM_COMMIT, PAGE_READWRITE);
                xassert(p == pRaw);
            }

            // construct the entry
            if constexpr ( std::is_constructible_v<entry> )
            {
                if constexpr ( sizeof...(T_ARGS) > 0 )
                {
                    new(&m_pEntries[m_Count]) entry{ std::forward<T_ARGS>(Args)... };
                }
                else
                {
                    new(&m_pEntries[m_Count]) entry;
                }
            }

            return ++m_Count;
        }

        template< typename...T_ARGS >
        std::size_t appendList( int Count, T_ARGS&&... Args) noexcept
        {
            const auto NewCount = m_Count + Count;
            xassert(NewCount < max_entries_v);

            // construct the entry
            if constexpr (std::is_constructible_v<entry>)
            {
                for( int i = m_Count; i ; --i )
                    append( std::forward<T_ARGS>(Args)... );
            }

            return m_Count;
        }

        bool deleteBySwap(std::size_t Index) noexcept
        {
            bool Ret = false;
            xassert(Index < m_Count);
            --m_Count;
            auto& A = m_pEntries[Index];
            if (Index != m_Count)
            {
                auto& B = m_pEntries[m_Count];
                A = std::move(B);
                Ret = true;
            }
            else if constexpr (std::is_destructible_v<entry>)
            {
                std::destroy_at(&A);
            }

            const auto ByteOffset = m_Count * sizeof(entry);
            const auto k          = (ByteOffset + sizeof(entry)) >> s_PageSizePow2;
            if (m_Count == 0 || k != (ByteOffset >> s_PageSizePow2))
            {
                auto pRaw = &reinterpret_cast<std::byte*>(m_pEntries)[k * s_PageSize];
                auto b    = VirtualFree(pRaw, s_PageSize, MEM_DECOMMIT);
                xassert(b);
            }

            return Ret;
        }

        template< typename T >
        auto& operator[](T Index) noexcept
        {
            xassert(Index<m_Count);
            return m_pEntries[Index];
        }

        template< typename T >
        constexpr auto& operator[](T Index) const noexcept
        {
            xassert(Index < m_Count);
            return m_pEntries[Index];
        }

        void clear(void) noexcept
        {
            if constexpr (std::is_destructible_v<entry>)
            {
                for( int i=0; i<m_Count; ++i )
                    std::destroy_at(&m_pEntries[i]);
            }
            m_Count = 0;
        }

    protected:

        entry*                          m_pEntries      {nullptr};
        std::size_t                     m_Count         { 0 };
        static inline int               s_PageSize      { 0 };
        static inline int               s_PageSizePow2  { 0 };
    };
}