
namespace engprog::quantum_containers
{
    // This pool has a two state threading model
    // First state is (update-state) second state is the (resolved-state)
    // This version only deals with the update-state and only for the append size
    template< std::size_t T_MAX_ENTRIES_V, typename...T_TYPES >
    class pool_v3
    {
    public:

        // A type cant be larger than 64K (or we will need to change the type of sizes_v)
        static_assert(((sizeof(T_TYPES) <= 0xffff) && ...));

        // We cant have more than 64 different types
        static_assert(sizeof...(T_TYPES) <= 64);

        constexpr static auto           page_size_v     = 4096;
        constexpr static auto           page_size_pow_v = xcore::bits::Log2Int(page_size_v);
        constexpr static std::size_t    max_entries_v   = T_MAX_ENTRIES_V;
        using                           self            = pool_v3;

        struct iterator
        {
            self&       m_This;
            std::size_t m_I;

            inline      auto operator   ++  (void)                    noexcept { m_I++; return *this; }
            constexpr   bool operator   !=  (std::nullptr_t)  const   noexcept { return m_This.m_Count != m_I; }
            constexpr   auto operator   *   (void)            const   noexcept { return std::tuple<const T_TYPES&...> { std::get<T_TYPES*>(m_This.m_Pointers)[m_I]... }; }
            constexpr   auto operator   *   (void)                    noexcept { return std::tuple<T_TYPES&...>       { std::get<T_TYPES*>(m_This.m_Pointers)[m_I]... }; }
        };

    public:

        constexpr   auto begin()                        const   noexcept { return iterator{ *this, 0 }; }
                    auto begin()                                noexcept { return iterator{ *this, 0 }; }
        constexpr   auto end()                          const   noexcept { return nullptr; }
        constexpr   auto size()                         const   noexcept { return m_Count; }

        pool_v3()
        {
            static bool b = true;
            if (b)
            {
                SYSTEM_INFO sSysInfo;
                GetSystemInfo(&sSysInfo);
                b = false;
                xassert(page_size_v <= sSysInfo.dwPageSize);
            }

            auto Init = [&](auto& P) constexpr
            {
                using       t           = xcore::types::decay_full_t<decltype(P)>;
                std::size_t PageCount   = (max_entries_v * sizeof(t)) / page_size_v;
                PageCount               = ((PageCount * page_size_v) < (max_entries_v * sizeof(t))) ? PageCount + 1 : PageCount;
                xassert((PageCount * page_size_v) >= (max_entries_v * sizeof(t)));

                P = reinterpret_cast<t*>(VirtualAlloc(nullptr, PageCount * page_size_v, MEM_RESERVE, PAGE_NOACCESS));
            };

            ((Init(std::get<T_TYPES*>(m_Pointers))), ...);
        }

        ~pool_v3(void) noexcept
        {
            clear();

            auto FreePerType = [&](auto& P) constexpr
            {
                using t = xcore::types::decay_full_t<decltype(P)>;
                if (P) VirtualFree(P, 0, MEM_RELEASE);
                P = nullptr;
            };

            ((FreePerType(std::get<T_TYPES*>(m_Pointers))), ...);
        }

        template< typename...T_ARGS >
        std::size_t append(T_ARGS&&... Args) noexcept
        {
            // We want arguments for all parameters or not arguments at all
            static_assert(sizeof...(T_ARGS) == 0
                || sizeof...(T_ARGS) == sizeof...(T_TYPES));

            using                       types_tuple     = std::tuple<T_TYPES...>;
            using                       offset_array    = std::array<std::uint32_t, sizeof...(T_TYPES)>;
            constexpr static std::array sizes_v         = { static_cast<std::uint16_t>(sizeof(T_TYPES))... };

            counter             NewCount;
            std::uint64_t       Types;
            offset_array        Offsets;

            //
            // Compute the Offsets, and bit Types that need to be allocated and determine if we need to lock
            //
            do
            {
                counter CaptureCount = m_NewCount.load(std::memory_order_relaxed);

                if (CaptureCount.m_Lock)
                {
                    XCORE_PERF_ZONE_SCOPED_NC("Pool::append Lock is Spinning and waiting", tracy::Color::ColorType::Red);
                    do
                    {
                        CaptureCount = m_NewCount.load(std::memory_order_relaxed);
                    } while (CaptureCount.m_Lock);
                }

                constexpr static auto   AppendPerType = [](auto& Types, const auto Count, const auto Index, offset_array& ArrayOffsets) constexpr noexcept
                {
                    const auto ByteOffset = Count * sizes_v[Index];
                    const auto k          = (ByteOffset + sizes_v[Index]) >> page_size_pow_v;
                    if (Count == 0 || k != (ByteOffset >> page_size_pow_v))
                    {
                        ArrayOffsets[Index]  = static_cast<std::uint32_t>(k);
                        Types               |= (1ull << Index);
                    }
                };

                Types = 0;
                ((AppendPerType(Types, CaptureCount.m_Count, xcore::types::tuple_t2i_v<T_TYPES, types_tuple>, Offsets)), ...);

                NewCount.m_Count = CaptureCount.m_Count + 1;
                NewCount.m_Lock  = !!Types;

                if (m_NewCount.compare_exchange_weak(CaptureCount, NewCount)) break;

            } while (true);

            //
            // Allocate pages if we have to
            //
            if (Types)
            {
                xassert(NewCount.m_Lock);
                constexpr static auto AllocPerType = []( std::byte* pRaw, const std::size_t Offset ) constexpr noexcept
                {
                    auto pPageAddress = &pRaw[Offset * page_size_v];
                    auto p            = VirtualAlloc(pPageAddress, page_size_v, MEM_COMMIT, PAGE_READWRITE);
                    xassert(p == pPageAddress);
                    return false;
                };

                ((
                    (
                        (Types & (1ull << xcore::types::tuple_t2i_v<T_TYPES, types_tuple>))
                        && AllocPerType
                        (
                            reinterpret_cast<std::byte*>(std::get<T_TYPES*>(m_Pointers))
                            , Offsets[xcore::types::tuple_t2i_v<T_TYPES, types_tuple>]
                        )
                        )
                    || ...
                    ));

                // Release the lock
                NewCount.m_Lock = 0;
                m_NewCount.store(NewCount, std::memory_order_relaxed);
            }

            //
            // Call the constructor if we need to
            //
            if constexpr ((std::is_constructible_v<T_TYPES> || ...))
            {
                const auto              Count            = NewCount.m_Count - 1;

                if constexpr ( sizeof...(T_ARGS) == 0 )
                {
                    static constexpr auto   ConstructPerType = []( auto& P ) constexpr noexcept
                    {
                        using t = xcore::types::decay_full_t<decltype(P)>;
                        if constexpr (std::is_constructible_v<t>) new(&P) t;
                    };
                    ((ConstructPerType(std::get<T_TYPES*>(m_Pointers)[Count])), ...);
                }
                else
                {
                    static constexpr auto   ConstructPerType = [](auto& P, auto&& Arg) constexpr noexcept
                    {
                        using t = xcore::types::decay_full_t<decltype(P)>;
                        new(&P) t{ Arg };
                    };
                    ((ConstructPerType(std::get<T_TYPES*>(m_Pointers)[Count], Args)), ...);
                }

                return Count;
            }
            else
            {
                return NewCount.m_Count - 1;
            }
        }


        void MemoryBarrier( void )
        {
            auto CaptureNewCount = m_NewCount.load(std::memory_order_relaxed);

            xassert(CaptureNewCount.m_Lock == 0);

            // Update the count
            m_Count = CaptureNewCount.m_Count;

            // Delete entries if any
            if( m_DeleteList.size() )
            {
                std::sort(m_DeleteList.begin(), m_DeleteList.end(), std::greater<std::uint32_t>());
                std::uint32_t LastEntry = ~0;
                for( auto& e : m_DeleteList )
                {
                    // Make sure we dont delete duplicates
                    if( e == LastEntry ) continue;
                    LastEntry = e;

                    // Delete the actual entry
                    deleteBySwapLinear(e);
                }

                // Reset the new count
                CaptureNewCount.m_Count = m_Count;
                m_NewCount.store(CaptureNewCount, std::memory_order_relaxed);

                // clear all the delete list
                m_DeleteList.clear();
            }
        }

        template< typename T_TYPE, typename T_INDEX >
        auto& operator[](T_INDEX Index) noexcept
        {
            xassert(Index >= 0);
            xassert(Index < m_Count);
            return std::get<T_TYPE*>()[Index];
        }

        template< typename T_TYPE, typename T_INDEX >
        constexpr auto& operator[](T_INDEX Index) const noexcept
        {
            xassert(Index >= 0);
            xassert(Index < m_Count);
            return std::get<T_TYPE*>()[Index];
        }

        template< typename T_INDEX >
        auto operator[](T_INDEX Index) noexcept
        {
            xassert(Index >= 0);
            xassert( static_cast<std::uint32_t>(Index) < m_Count);
            return std::tuple<T_TYPES& ... >( std::get<T_TYPES*>(m_Pointers)[Index] ... );
        }

        template< typename T_INDEX >
        void deleteBySwap( T_INDEX Index ) noexcept
        {
            xassert(Index >= 0 );
            m_DeleteList.append(static_cast<std::uint32_t>(Index));
        }

        void clear(void) noexcept
        {
            if constexpr ( (std::is_destructible_v<T_TYPES> && ...) )
            {
                auto ClearPerType = [&](auto& P) constexpr
                {
                    using t = xcore::types::decay_full_t<decltype(P)>;
                    if constexpr (std::is_destructible_v<t>)
                    {
                        for (auto i = 0u; i < m_Count; ++i)
                            std::destroy_at(&P[i]);
                    }
                };

                ((ClearPerType(std::get<T_TYPES*>(m_Pointers))), ...);
            }

            m_Count = 0;
        }

    protected:

        union counter
        {
            std::uint32_t       m_FullValue;
            struct
            {
                std::uint32_t   m_Count:31
                            ,   m_Lock:1;
            };
        };

    protected:

        void deleteBySwapLinear(std::size_t Index) noexcept
        {
            xassert(Index < m_Count);
            --m_Count;
            if (Index != m_Count)
            {
                auto SwapDeletePerType = [&](auto& P) constexpr
                {
                    using t = xcore::types::decay_full_t<decltype(P)>;

                    P[Index] = P[m_Count];

                    const auto ByteOffset = m_Count * sizeof(t);
                    const auto k          = (ByteOffset + sizeof(t)) >> page_size_pow_v;
                    if (m_Count == 0 || k != (ByteOffset >> page_size_pow_v))
                    {
                        auto pRaw = &reinterpret_cast<std::byte*>(P)[k * page_size_v];
                        auto b    = VirtualFree(pRaw, page_size_v, MEM_DECOMMIT);
                        xassert(b);
                    }
                };

                ((SwapDeletePerType(std::get<T_TYPES*>(m_Pointers))), ...);
            }
            else
            {
                auto DeletePerType = [&](auto& P) constexpr
                {
                    using t = xcore::types::decay_full_t<decltype(P)>;
                    if constexpr (std::is_destructible_v<t>)
                    {
                        std::destroy_at(&P[Index]);
                    }

                    const auto ByteOffset = m_Count * sizeof(t);
                    const auto k          = (ByteOffset + sizeof(t)) >> page_size_pow_v;
                    if (m_Count == 0 || k != (ByteOffset >> page_size_pow_v))
                    {
                        auto pRaw = &reinterpret_cast<std::byte*>(P)[k * page_size_v];
                        auto b    = VirtualFree(pRaw, page_size_v, MEM_DECOMMIT);
                        xassert(b);
                    }
                };

                ((DeletePerType(std::get<T_TYPES*>(m_Pointers))), ...);
            }
        }

    protected:

        std::atomic<counter>                        m_NewCount       {{0}};
        std::uint32_t                               m_Count          {0};
        std::tuple<T_TYPES* ... >                   m_Pointers       {};
        pool_v1<std::uint32_t, max_entries_v>       m_DeleteList     {};
    };
}