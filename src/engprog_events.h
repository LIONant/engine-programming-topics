namespace engprog::event
{
    namespace details
    {
        //----------------------------------------------------------------------------------------------------

        template< auto T_MEMBER_DECLARATION, typename T_EVENT, typename T_CLASS = typename xcore::function::traits<decltype(T_MEMBER_DECLARATION)>::class_type >
        class delegate
        {
        public:

            using event_t = T_EVENT;
            using class_t = T_CLASS;

            xforceinline constexpr delegate(class_t* pThis, event_t& Event) noexcept :
                m_pEven{ &Event }, m_pThis{ pThis }
            {
                xassert(m_pThis);
                m_pEven->append<T_MEMBER_DECLARATION>(m_pThis);
            }

            xforceinline constexpr delegate(class_t* pThis) noexcept :
                m_pThis{ pThis }
            {
                xassert(m_pThis);
            }

            xforceinline ~delegate() noexcept
            {
                if (m_pEven) m_pEven->remove(m_pThis);
            }

            xforceinline void setup(class_t* pThis, event_t& Event) noexcept
            {
                xassert(m_pThis == nullptr);
                xassert(m_pEven == nullptr);
                m_pEven = &Event;
                m_pThis = pThis;
                m_pEven->append<T_MEMBER_DECLARATION>(*m_pThis);
            }

            xforceinline void setup(event_t& Event) noexcept
            {
                xassert(m_pThis);
                m_pEven = &Event;
                m_pEven->append<T_MEMBER_DECLARATION>(*m_pThis);
            }

            xforceinline void clear(void) noexcept
            {
                if (m_pEven) m_pEven->remove(m_pThis);
                m_pEven = nullptr;
                m_pThis = nullptr;
            }

        protected:

            event_t* m_pEven{ nullptr };
            class_t* m_pThis{ nullptr };
        };

        //----------------------------------------------------------------------------------------------------

        template< auto T_MEMBER_DECLARATION, typename T_EVENT >
        class delegate< T_MEMBER_DECLARATION, T_EVENT, void >
        {
        public:

            using event_t = T_EVENT;

            constexpr delegate(void) noexcept = default;

            xforceinline constexpr delegate(event_t& Event) noexcept :
                m_pEven{ &Event }
            {
                m_pEven->append<T_MEMBER_DECLARATION>(reinterpret_cast<std::size_t>(this));
            }

            xforceinline ~delegate() noexcept
            {
                if (m_pEven) m_pEven->remove(reinterpret_cast<std::size_t>(this));
            }

            xforceinline void setup(event_t& Event) noexcept
            {
                xassert(m_pEven == nullptr);
                m_pEven = &Event;
                m_pEven->append<T_MEMBER_DECLARATION>(reinterpret_cast<std::size_t>(this));
            }

            xforceinline void clear(void) noexcept
            {
                if (m_pEven) m_pEven->remove(reinterpret_cast<std::size_t>(this));
                m_pEven = nullptr;
            }

        protected:

            event_t* m_pEven{ nullptr };
        };

    }

    //----------------------------------------------------------------------------------------------------
    // If you use the handle in the first argument in the event you will get the Key used to store the delegate
    // The NotifyAll must then must be fill the first argument with a default handle == ".NotifyAll( handle{}, ... )"
    //----------------------------------------------------------------------------------------------------
    struct handle
    {
        void* m_pValue;
    };

    //----------------------------------------------------------------------------------------------------
    // Type for the event
    //----------------------------------------------------------------------------------------------------
    template< typename T >
    class type;

    template< typename T_RET, class ...T_ARGS >
    class type< T_RET(T_ARGS...) >
    {
    public:

        using callback_real                 = T_RET(T_ARGS...);
        using callback                      = T_RET(void*, T_ARGS...);
        using self                          = type;

        template< auto T_MEMBER_DECLARATION >
        using delegate = details::delegate< T_MEMBER_DECLARATION, self >;

        template< auto T_MEMBER_DECLARATION, typename T_CLASS >
        inline void append(T_CLASS& Instance) noexcept
        {
            static_assert(xcore::function::traits_compare< xcore::function::traits<decltype(T_MEMBER_DECLARATION)>
                        , xcore::function::traits<callback_real>>::value);
            static_assert( std::is_pointer_v<T_CLASS> == false );
            static_assert( std::is_same_v< xcore::function::traits<decltype(T_MEMBER_DECLARATION)>::class_type, T_CLASS > );

            xcore::lock::scope Lk(m_LkDelegates);
            m_LkDelegates.get().push_back
            ({
                &Instance
                , [](void* This, T_ARGS... Args) constexpr noexcept
                {
                    if constexpr (std::is_same_v<void, T_RET>)
                    {
                        std::invoke( T_MEMBER_DECLARATION, reinterpret_cast<T_CLASS*>(This), std::forward<T_ARGS>(Args)...);
                    }
                    else
                    {
                        return std::invoke(T_MEMBER_DECLARATION, reinterpret_cast<T_CLASS*>(This), std::forward<T_ARGS>(Args)...);
                    }
                }
            });
        }

        template< auto T_FUNCTION_DECLARATION >
        inline void append(void* pUserPointer ) noexcept
        {
            static_assert(std::is_same_v< xcore::function::traits<decltype(T_FUNCTION_DECLARATION)>::class_type, void >);
            static_assert(xcore::function::traits_compare< xcore::function::traits<decltype(T_FUNCTION_DECLARATION)>
                        , xcore::function::traits<callback_real>>::value);

            xcore::lock::scope Lk(m_LkDelegates);
            m_LkDelegates.get().push_back
            ({
                pUserPointer
                , [](void*, T_ARGS... Args) constexpr noexcept
                {
                    if constexpr (std::is_same_v<void, T_RET>)
                    {
                        std::invoke(T_FUNCTION_DECLARATION, std::forward<T_ARGS>(Args)...);
                    }
                    else
                    {
                        return std::invoke(T_FUNCTION_DECLARATION, std::forward<T_ARGS>(Args)...);
                    }
                }
            });
        }

        template< auto T_FUNCTION_DECLARATION >
        inline void append(std::size_t UID) noexcept
        {
            append<T_FUNCTION_DECLARATION>(reinterpret_cast<void*>(UID));
        }

        template< auto T_FUNCTION_DECLARATION >
        xforceinline void append( void ) noexcept
        {
            append<T_FUNCTION_DECLARATION>(reinterpret_cast<void*>(T_FUNCTION_DECLARATION));
        }

        inline void remove(void* This) noexcept
        {
            if (This == nullptr) return;
            xcore::lock::scope Lk(m_LkDelegates);
            auto& Delegates = m_LkDelegates.get();
            for (auto& E : Delegates)
            {
                if (std::get<0>(E) == This)
                {
                    E = Delegates.back();
                    Delegates.pop_back();
                    break;
                }
            }
        }

        inline void remove(std::size_t ID) noexcept
        {
            remove(reinterpret_cast<void*>(ID));
        }

        template< auto T_FUNCTION_DECLARATION >
        inline void remove(void) noexcept
        {
            remove(T_FUNCTION_DECLARATION);
        }

        inline void NotifyAll(T_ARGS... Args) const noexcept
        {
            xcore::lock::scope Lk(m_LkDelegates);
            if constexpr (std::is_same_v< handle, std::tuple_element_t<0, std::tuple<T_ARGS..., int>>>)
            {
                [&]( handle, auto...LessArgs ) constexpr noexcept
                {
                    for (auto& [This, Callback] : m_LkDelegates.get())
                        if constexpr (std::is_same_v<void, T_RET>)
                            Callback(This, handle{ This }, LessArgs...);
                        else
                            return Callback(This, handle{ This }, LessArgs...);
                }(Args...);
            }
            else
            {
                for (auto& [This, Callback] : m_LkDelegates.get())
                    if constexpr (std::is_same_v<void, T_RET>)
                        Callback(This, std::forward<T_ARGS>(Args) ...);
                    else if (Callback(This, std::forward<T_ARGS>(Args) ...)) break;
            }
        }

    protected:

        xcore::lock::object<std::vector<std::tuple<void*, callback*>>, xcore::lock::semaphore > m_LkDelegates;
    };
}
