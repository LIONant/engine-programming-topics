
#include "engprog_quantum_containers_pool_v1.h"
#include "engprog_quantum_containers_pool_v2.h"
#include "engprog_quantum_containers_pool_v3.h"
#include "engprog_quantum_containers_hashmap.h"

namespace engprog::quantum_containers::examples
{
    //--------------------------------------------------------------------------------
    template< typename T >
    void Performance(const char* pName, T&& Function) noexcept
    {
        printf(pName);
        const auto t1 = std::chrono::high_resolution_clock::now();
        Function();
        const auto t2 = std::chrono::high_resolution_clock::now();
        const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        printf("Done: %f seconds\n", time_span.count());
    }

    //--------------------------------------------------------------------------------
    struct t1
    {
        xcore::vector3 L;
    };

    struct t2
    {
        xcore::vector3 L[3];
        std::array<std::byte, 16 > Array;
    };

    struct t3
    {
        const char* P;
    };

    constexpr static auto MAX_ENTRIES = 30000000 XCORE_CMD_DEBUG(-30000000 + 100000);

    //--------------------------------------------------------------------------------

    void TestQstp()
    {
        engprog::quantum_containers::pool_v1< t1, MAX_ENTRIES > Pool;
        xcore::scheduler::channel Channel(xconst_universal_str("Test PoolV1"));

        static constexpr auto Count = MAX_ENTRIES / 64;

        printf("---------------------------------------------------------------------------------\n");
        printf(" Testing Append for QSTP\n");
        printf("---------------------------------------------------------------------------------\n\n");

        // This is the only multicore function for this type of pool
        Performance("Allocation: ", [&]
        {
            for (int c = 0; c < MAX_ENTRIES; c += std::min(MAX_ENTRIES - c, Count))
            {
                Channel.SubmitJob([&Pool, c]
                    {
                        for (auto i = c, end = std::min(MAX_ENTRIES, c + Count); i < end; i++)
                        {
                            if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                            Pool.append( t1{ 0 } );
                        }
                    });
            }
            Channel.join();
            int a = 9;
        });
    }

    //--------------------------------------------------------------------------------

    void TestQMTMA_NoDelete()
    {
        engprog::quantum_containers::pool_v2<MAX_ENTRIES, xcore::matrix4, int, t1, t2, t3 > Pool;
        xcore::scheduler::channel Channel(xconst_universal_str("Test PoolV2"));

        static constexpr auto Count = MAX_ENTRIES / 64;

        printf("---------------------------------------------------------------------------------\n");
        printf(" Testing Append for QMTMA NoDelete\n");
        printf("---------------------------------------------------------------------------------\n\n");

        Performance("Allocation: ", [&]
            {
                for (int c = 0; c < MAX_ENTRIES; c += std::min(MAX_ENTRIES - c, Count))
                {
                    Channel.SubmitJob([&Pool, c]
                        {
                            for (auto i = c, end = std::min(MAX_ENTRIES, c + Count); i < end; i++)
                            {
                                if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                                Pool.append(xcore::matrix4::Identity(), i, t1{ 0 }, t2{ { 0, 0, 0 }, {} }, t3{ nullptr });
                            }
                        });
                }
                Channel.join();
                Pool.MemoryBarrier();
            });

        Performance("Iteration: ", [&]
            {
                for (int c = 0; c < MAX_ENTRIES; c += std::min(MAX_ENTRIES - c, Count))
                    Channel.SubmitJob([&Pool, c]
                        {
                            for (auto i = c, end = std::min(MAX_ENTRIES, c + Count); i < end; i++)
                            {
                                auto [Matrix, Int, T1, T2, T3] = Pool[i];
                                if ((i++ % (MAX_ENTRIES / 60)) == 0) printf(".");
                                Matrix = xcore::matrix4::Identity();
                                Int += Int;
                                T1.L += T1.L;
                                T2.L[0] += T1.L[0];
                                T2.L[1] += T2.L[0];
                                T2.L[2] += T1.L[0];
                                T3.P = "Hello";
                            }
                        });
                Channel.join();
            });

    }

    //--------------------------------------------------------------------------------

    void TestQMTMA()
    {
        engprog::quantum_containers::pool_v3<MAX_ENTRIES, xcore::matrix4, int, t1, t2, t3 > Pool;
        xcore::scheduler::channel Channel(xconst_universal_str("Test PoolV2"));

        printf("---------------------------------------------------------------------------------\n");
        printf(" Testing Append for QMTMA\n");
        printf("---------------------------------------------------------------------------------\n\n");

        Performance("Big Test: ", [&]
            {
                static constexpr auto BlockSize = MAX_ENTRIES / 64;

                xcore::random::small_generator GeneratorSeeder;
                GeneratorSeeder.setSeed64(234454212323456);

                for( int c=0; c < MAX_ENTRIES; c += BlockSize )
                {
                    static constexpr auto nJobs         = 64;
                    static constexpr auto EntriesPerJob = BlockSize / nJobs;

                    printf(".");

                    for (int j = 0; j < nJobs; j++ )
                    {
                        // Lets add some entries
                        Channel.SubmitJob([ &Pool ]
                            {
                                for ( auto i = 0; i < EntriesPerJob; i++ )
                                {
                                    Pool.append(xcore::matrix4::Identity(), i, t1{ 0 }, t2{ { 0, 0, 0 }, {} }, t3{ nullptr });
                                }
                            });

                        // Lets also iterate if we can
                        {
                            static constexpr auto iterateRange = 10000;

                            // Note that unless we have non-overlapping ranges here we will causing problems as two threads will
                            // be overwriting each-others data
                            if (Pool.size()) Channel.SubmitJob([ &Pool, iStart = j*iterateRange, iCount = static_cast<std::size_t>( std::max( 0, std::min<int>(iterateRange, Pool.size() - j*iterateRange))) ]
                                {
                                    for ( int i=0; i<iCount; i++ )
                                    {
                                        const auto Index = i + iStart;
                                              auto [Matrix, Int, T1, T2, T3] = Pool[Index];

                                        Matrix = xcore::matrix4::Identity();
                                        Int += Int;
                                        T1.L += T1.L;
                                        T2.L[0] += T1.L[0];
                                        T2.L[1] += T2.L[0];
                                        T2.L[2] += T1.L[0];
                                        T3.P = "Hello";
                                    }
                                });
                        }

                        // Lets also delete a few random entries
                        if(Pool.size()) Channel.SubmitJob([&Pool, Seed = GeneratorSeeder.RandU32() ]
                            {
                                xcore::random::small_generator Rnd;
                                Rnd.setSeed32(Seed);

                                // Delete 1000 random entries
                                for( int j=0; j<1000; j++ )
                                {
                                    auto Index = Rnd.RandS32( 0, Pool.size() );
                                    Pool.deleteBySwap(Index);
                                }
                            });
                    }

                    //
                    // Sync everything
                    //
                    Channel.join();
                    Pool.MemoryBarrier();
                }
        });
    }

    //--------------------------------------------------------------------------------

    void HashTable()
    {
        using myguid = xcore::guid::unit<64>;

        struct state
        {
            myguid m_Key;           
            bool   m_isInUse;       // Note that this is in linear space
        };

        engprog::quantum_containers::hashmap::fixed< myguid, std::uint32_t, MAX_ENTRIES > HashMap;
        engprog::quantum_containers::pool_v1< state,  MAX_ENTRIES >                       Entries;

        static constexpr auto nJobs         = 64;
        static constexpr auto EntriesPerJob = MAX_ENTRIES / nJobs;

        printf("---------------------------------------------------------------------------------\n");
        printf(" Testing Append for Hashmap\n");
        printf("---------------------------------------------------------------------------------\n\n");

        //
        // Initialize all the entries
        //
        {
            xcore::scheduler::channel Channel(xconst_universal_str("Hashmap::Init-Entries"));
            for (int j = 0; j < nJobs; j++) Channel.SubmitJob([&Entries, j]
            {
                for (auto i = 0; i < EntriesPerJob; i++)
                {
                    myguid gEntry(xcore::not_null);
                    Entries.append( state{ gEntry, false } );
                }
            });
        }

        //
        // Check the performance of allocation
        //
        {
            xcore::scheduler::channel Channel(xconst_universal_str("Hashmap::Alloc"));
            Performance("Hashmap::Alloc: ", [&]
            {
                for (int j = 0; j < nJobs; j++)
                {
                    // Lets add some entries
                    Channel.SubmitJob([&HashMap, &Entries, j]
                    {
                        printf(".");
                        for (auto i = 0; i < EntriesPerJob; i++)
                        {
                            auto  Index     = j * EntriesPerJob + i;
                            auto& Key       = Entries[Index].m_Key;
                            auto pCollision = HashMap.alloc( Key, [=](std::uint32_t& V)
                            {
                                V = Index;
                            });
                            xassert(pCollision==nullptr);
                        }
                    });
                }
                Channel.join();
            });
        }

        //
        // Check the performance of finding the entry
        //
        {
            xcore::scheduler::channel Channel(xconst_universal_str("Hashmap::Find"));
            Performance("Hashmap::find: ", [&]
            {
                for (int j = 0; j < nJobs; j++)
                {
                    // Lets add some entries
                    Channel.SubmitJob([&HashMap, &Entries, j]
                    {
                        printf(".");
                        for (auto i = 0; i < EntriesPerJob; i++)
                        {
                            auto Index   = j * EntriesPerJob + i;
                            bool isFound = HashMap.find( Entries[Index].m_Key, [=](const std::uint32_t& V)
                            {
                                xassert( V == Index );
                            });
                            xassert(isFound);
                        }
                    });
                }
                Channel.join();
            });
        }

        //
        // Check the performance of free the entry
        //
        {
            xcore::scheduler::channel Channel(xconst_universal_str("Hashmap::Free"));
            Performance("Hashmap::free: ", [&]
            {
                for (int j = 0; j < nJobs; j++)
                {
                    // Lets delete some entries
                    Channel.SubmitJob([&HashMap, &Entries, j]
                    {
                        printf(".");
                        for (auto i = 0; i < EntriesPerJob; i++)
                        {
                            auto Index   = j * EntriesPerJob + i;
                            bool isFound = HashMap.free( Entries[Index].m_Key );
                            xassert(isFound);
                        }
                    });
                }
                Channel.join();
            });
        }

        //
        // Running everything in parallel
        //
        {
           // engprog::quantum_containers::hashmap::fixed< myguid, std::uint32_t, MAX_ENTRIES > HashMap;
            xcore::random::small_generator GeneratorSeeder;
            GeneratorSeeder.setSeed64(234454212323456);

            xcore::scheduler::channel Channel(xconst_universal_str("Hashmap::FullStress"));
            Performance("Hashmap::Full Stress: ", [&]
            {
                for (int j = 0; j < 10000; j++)
                {
                    Channel.SubmitJob([&HashMap, &Entries, j, Seed = GeneratorSeeder.RandU32() ]
                    {
                        if( (j%150) == 0 ) printf(".");

                        xcore::random::small_generator GeneratorSeeder;
                        GeneratorSeeder.setSeed32(Seed);

                        // Try adding random nodes nodes
                        for( int i=0, end = GeneratorSeeder.RandS32(0, 10000); i<end; i++ )
                        {
                            std::uint32_t Index = GeneratorSeeder.RandS32(0, Entries.size()-1);
                            if( Entries[Index].m_isInUse == false )
                                HashMap.alloc(Entries[Index].m_Key, [&]( std::uint32_t& V ){ V = Index; Entries[Index].m_isInUse = true; } );
                        }
                        
                        // Try Iterating nodes
                        for (int i = 0, end = GeneratorSeeder.RandS32(0, 10000); i < end; i++)
                        {
                            std::uint32_t Index = GeneratorSeeder.RandS32(0, Entries.size() - 1);
                            std::as_const(HashMap).find( Entries[Index].m_Key );
                        }

                        // Try deleting random nodes nodes
                        for (int i = 0, end = GeneratorSeeder.RandS32(0, 10000); i < end; i++)
                        {
                            std::uint32_t Index = GeneratorSeeder.RandS32(0, Entries.size() - 1);
                            if (Entries[Index].m_isInUse == true)
                                HashMap.free( Entries[Index].m_Key, [&]( const std::uint32_t& ) { Entries[Index].m_isInUse = false; });
                        }
                    });
                }
                Channel.join();
            });
        }
    }

    //--------------------------------------------------------------------------------
    // Test
    //--------------------------------------------------------------------------------
    void Test() noexcept
    {
        printf("#################################################################################\n");
        printf("# Profiling Quantum Containers\n");
        printf("#################################################################################\n\n");

        //
        // Multiple Type pools
        //
        if(0) { TestQstp();           printf("\n\n"); }
        if(0) { TestQMTMA_NoDelete(); printf("\n\n"); }
        if(0) { TestQMTMA();          printf("\n\n"); }
        if(1) { HashTable();          printf("\n\n"); }

        printf("\n#################################################################################\n");
        printf("# Done\n");
        printf("#################################################################################\n");
    }
}