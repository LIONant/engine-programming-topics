
namespace engprog::multi_threaded::examples
{
    //--------------------------------------------------------------------------------
    template< typename T >
    void Performance(const char* pName, T&& Function) noexcept
    {
        printf(pName);
        const auto t1 = std::chrono::high_resolution_clock::now();
        Function();
        const auto t2 = std::chrono::high_resolution_clock::now();
        const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        printf("Done: %f seconds\n", time_span.count());
    }

    static constexpr auto max_entries_v = 10000000;

    //--------------------------------------------------------------------------------
    void Test1()
    {
        xcore::lock::object< std::vector<int>, xcore::lock::semaphore > X;
        {
            xcore::scheduler::channel MyChannel(xconst_universal_str("Multicore::Test1"));

            for (int i = 0; i < max_entries_v; i++)
            {
                MyChannel.SubmitJob([&X,i]
                    {
                        xcore::lock::scope Lk(X);
                        for (int j = i, end = std::min(i + 10000, max_entries_v ); j < end; j++)
                            X.get().push_back(5);
                    });
            }

            MyChannel.join();
         }
    }

    //--------------------------------------------------------------------------------
    void Test2()
    {
        xcore::lock::object< std::vector<int>, xcore::lock::semaphore > X;
        {
            xcore::scheduler::channel MyChannel(xconst_universal_str("Multicore::Test2"));

            for (int i = 0; i < max_entries_v; i += 10000)
            {
                MyChannel.SubmitJob( [&X,i]
                {
                    xcore::lock::scope Lk(X);
                    for (int j = i, end = std::min(i + 10000, max_entries_v ); j < end; j++)
                    {
                        X.get().push_back(5);
                    }
                });
            }

            MyChannel.join();

            xcore::lock::object< int, xcore::lock::spin > Total(0);

            for (int i = 0; i < max_entries_v; i += 10000 )
            {
                MyChannel.SubmitJob([&X, &Total, i]
                {
                    int LocalTotal = 0;

                    {
                        xcore::lock::scope Lk(std::as_const(X));
                        for (int j = i, end = std::min(i + 10000, max_entries_v); j < end; j++)
                            LocalTotal += X.get()[j];
                    }

                    xcore::lock::scope Lk( Total );
                    Total.get() += LocalTotal;
                });
            }

            MyChannel.join();
        }
    }

    //--------------------------------------------------------------------------------
    void Test3()
    {
        xcore::lock::object< std::vector<int>, xcore::lock::semaphore > X;

        {
            xcore::scheduler::channel MyChannel(xconst_universal_str("Multicore::Test3"));

            for (int i = 0; i < max_entries_v; i += 10000)
            {
                MyChannel.SubmitJob([&X, i]
                    {
                        xcore::lock::scope Lk(X);
                        for (int j = i, end = std::min(i + 10000, max_entries_v); j < end; j++)
                        {
                            X.get().push_back(5);
                        }
                    });
            }

            MyChannel.join();

            std::atomic<int> QTotal{0};
            {
                xcore::lock::scope ReadOnlyLock( std::as_const(X) );
                for (int i = 0; i < max_entries_v; i += 10000)
                {
                    MyChannel.SubmitJob([&X, &QTotal, i]
                        {
                            for (int j = i, end = std::min(i + 10000, max_entries_v); j < end; j++)
                            {
                                auto CaptureVar = QTotal.load( std::memory_order_relaxed );
                                do
                                {
                                    auto NewVar = CaptureVar + X.get()[j];
                                    if( QTotal.compare_exchange_weak(CaptureVar, NewVar) ) break;
                                } while( true );
                            }
                                
                        });
                }
                MyChannel.join();
            }
        }
    }

    //--------------------------------------------------------------------------------
    void Test4()
    {
        std::vector<int> X;

        xcore::scheduler::channel MyChannel(xconst_universal_str("Multicore::Test4"));

        X.resize(max_entries_v);

        for (int i = 0; i < max_entries_v; i += 10000)
        {
            MyChannel.SubmitJob([ X = std::span{ &X[i], static_cast<std::size_t>(std::min( 10000, max_entries_v - i)) }]
                {
                    for (auto& e : X )
                    {
                        e = 5;
                    }
                });
        }

        MyChannel.join();

        std::atomic<int> QTotal{ 0 };
        for (int i = 0; i < max_entries_v; i += 10000)
        {
            MyChannel.SubmitJob([ ConstX = std::span{ &std::as_const(X)[i], static_cast<std::size_t>(std::min(10000, max_entries_v-i)) }
                                , &QTotal]
                {
                    for (auto& e : ConstX )
                    {
                        auto CaptureVar = QTotal.load(std::memory_order_relaxed);
                        do
                        {
                            auto NewVar = CaptureVar + e;
                            if (QTotal.compare_exchange_weak(CaptureVar, NewVar)) break;
                        } while (true);
                    }

                });
        }

        MyChannel.join();
    }

    //--------------------------------------------------------------------------------
    void Test5()
    {
        std::vector<int> X;

        xcore::scheduler::channel MyChannel(xconst_universal_str("Multicore::Test5"));

        X.resize(max_entries_v);

        for (int i = 0; i < max_entries_v; i += 10000)
        {
            MyChannel.SubmitJob([X = std::span{ &X[i], static_cast<std::size_t>(std::min( 10000, max_entries_v - i)) }]
                {
                    for (auto& e : X)
                    {
                        e = 5;
                    }
                });
        }

        MyChannel.join();

        std::atomic<int> QTotal{ 0 };
        for (int i = 0; i < max_entries_v; i += 10000)
        {
            MyChannel.SubmitJob([ConstX = std::span{ &std::as_const(X)[i], static_cast<std::size_t>(std::min( 10000, max_entries_v - i)) }
                , &QTotal]
                {
                    int LocalTotal = 0;
                    for (auto& e : ConstX)
                    {
                        LocalTotal += e;
                    }

                    auto CaptureVar = QTotal.load(std::memory_order_relaxed);
                    do
                    {
                        auto NewVar = CaptureVar + LocalTotal;
                        if (QTotal.compare_exchange_weak(CaptureVar, NewVar)) break;
                    } while (true);

                });
        }

        MyChannel.join();
    }


    //------------------------------------------------------------------------

    void Test()
    {
        if (0) Performance( "Test1  ", Test1 );
        if (1) Performance( "Test2  ", Test2 );
        if (1) Performance( "Test3  ", Test3 );
        if (1) Performance( "Test4  ", Test4 );
        if (1) Performance( "Test5  ", Test5 );
    }
}