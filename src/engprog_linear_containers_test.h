#include <windows.h>
#include <memoryapi.h>

#include "xcore_assert.h"

#include "engprog_linear_containers_pool_v5.h"
#include "engprog_linear_containers_pool_v4.h"
#include "engprog_linear_containers_pool_v3.h"
#include "engprog_linear_containers_pool_v2.h"
#include "engprog_linear_containers_pool_v1.h"




namespace engprog::linear_containers::examples
{
    //--------------------------------------------------------------------------------
    template< typename T >
    void Performance(const char* pName, T&& Function) noexcept
    {
        printf(pName);
        const auto t1 = std::chrono::high_resolution_clock::now();
        Function();
        const auto t2 = std::chrono::high_resolution_clock::now();
        const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        printf("Done: %f seconds\n", time_span.count());
    }

    //--------------------------------------------------------------------------------
    struct t1
    {
        xcore::vector3 L;
    };

    struct t2
    {
        xcore::vector3 L[3];
        std::array<std::byte, 16 > Array;
    };

    struct t3
    {
        const char* P;
    };

    //--------------------------------------------------------------------------------

    template< template< std::size_t, typename...T> class T >
    void ProfileMultiple()
    {
        constexpr static auto MAX_ENTRIES = 30000000 XCORE_CMD_DEBUG(-30000000 + 10000);

        T<MAX_ENTRIES, xcore::matrix4, int, t1, t2, t3 > Pool;
        Performance("Allocation: ", [&]
            {
                for (int i = 0; i < MAX_ENTRIES; i++)
                {
                    if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                    Pool.append(xcore::matrix4::Identity(), i, t1{ 0 }, t2{ { 0, 0, 0 }, {} }, t3{ nullptr });
                }
            });

        Performance("Iteration: ", [&]
            {
                int i = 0;
                for (auto [Matrix, Int, T1, T2, T3] : Pool)
                {
                    if ((i++ % (MAX_ENTRIES / 60)) == 0) printf(".");
                    Matrix = xcore::matrix4::Identity();
                    Int += Int;
                    T1.L += T1.L;
                    T2.L[0] += T1.L[0];
                    T2.L[1] += T2.L[0];
                    T2.L[2] += T1.L[0];
                    T3.P = "Hello";
                }
            });

        Performance("Deletion: ", [&]
            {
                for (int i = 0; i < MAX_ENTRIES; i++)
                {
                    if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                    Pool.deleteBySwap(0);
                }
            });
    }

    //--------------------------------------------------------------------------------

    void ProfileMultipleTypesPools()
    {
        printf("\n#################################################################################\n");
        printf("# Profiling Multiple Types Containers\n");
        printf("#################################################################################\n\n");

        //
        // Test pool v5
        //
        {
            printf("\nVersion 5, VM Per page Streams\n");
            ProfileMultiple<pool_v5>();
        }

        //
        // Test pool v4
        //
        {
            printf("\nVersion 4, VM Traditional structure\n");
            ProfileMultiple<pool_v4>();
        }

        //
        // Test pool v3
        //
        {
            printf("\nVersion 3, VM Separate allocations per Type\n");
            ProfileMultiple<pool_v3>();
        }
    }

    //--------------------------------------------------------------------------------

    template< std::size_t MAX_ENTRIES, typename T >
    void ProfileSinge()
    {
        T Pool;

        Performance("Allocation: ", [&]
            {
                for (int i = 0; i < MAX_ENTRIES; i++)
                {
                    if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                    Pool.append(xcore::matrix4::Identity());
                }
            });

        Performance("Iteration: ", [&]
            {
                int i = 0;
                for (auto& E : Pool)
                {
                    if ((i++ % (MAX_ENTRIES / 60)) == 0) printf(".");
                    E = xcore::matrix4::Identity();
                }
            });

        Performance("Deletion: ", [&]
            {
                for (int i = 0; i < MAX_ENTRIES; i++)
                {
                    if ((i % (MAX_ENTRIES / 60)) == 0) printf(".");
                    Pool.deleteBySwap(0);
                }
            });
    }

    //--------------------------------------------------------------------------------
    void ProfileSingleTypePools()
    {
        constexpr static auto       Count = 60000000 XCORE_CMD_DEBUG(-60000000 + 10000);
        printf("\n#################################################################################\n");
        printf("# Profiling Single Type Containers\n");
        printf("#################################################################################\n\n");

        //
        // Test pool v2
        //
        {
            printf("\nVersion 2, VM Pool\n");
            ProfileSinge< Count, pool_v2<xcore::matrix4, Count >>();
        }

        //
        // Test pool
        //
        {
            printf("\nVersion 1, Paged Pool\n");
            ProfileSinge< Count, pool_v1<xcore::matrix4>>();
        }

        //
        // Test vector
        //
        {
            printf("\nVector Pool\n");
            std::vector<xcore::matrix4> Vector;

            Performance("Vector Allocation: ", [&]
                {
                    for (int i = 0; i < Count; i++)
                    {
                        if ((i % (Count / 60)) == 0) printf(".");
                        Vector.push_back(xcore::matrix4::Identity());
                    }
                });

            Performance("Vector Iteration: ", [&]
                {
                    int i = 0;
                    for (auto& E : Vector)
                    {
                        if ((i++ % (Count / 60)) == 0) printf(".");
                        E = xcore::matrix4::Identity();
                    }
                });

            Performance("Vector Deletion: ", [&]
                {
                    for (int i = 0; i < Count; i++)
                    {
                        if ((i % (Count / 60)) == 0) printf(".");
                        Vector.erase(Vector.begin());
                    }
                });
        }
    }

    //--------------------------------------------------------------------------------
    // Test
    //--------------------------------------------------------------------------------
    void Test() noexcept
    {
        printf("#################################################################################\n");
        printf("# Profiling Containers\n");
        printf("#################################################################################\n\n");

        //
        // Multiple Type pools
        //
        ProfileMultipleTypesPools();

        //
        // Single Type pools
        //
        ProfileSingleTypePools();


        printf("\n#################################################################################\n");
        printf("# Done\n");
        printf("#################################################################################\n");
    }
}