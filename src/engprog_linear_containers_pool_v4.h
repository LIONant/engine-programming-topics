
namespace engprog::linear_containers
{
    // VM Multi-type structure based.
    // All the elements of the type packs are collected into a structure
    template< std::size_t T_MAX_ENTRIES_V, typename...T_TYPES >
    class pool_v4
    {
    public:

        using                           self                = pool_v4;
        constexpr static auto           max_entries_v       = T_MAX_ENTRIES_V;
        constexpr static auto           offset_tuple_v      = []() constexpr
        {
            std::array<std::uint32_t, sizeof...(T_TYPES)>   Offsets{};

            std::uint32_t           p                       = 0;
            const auto              Aligments               = std::array{ static_cast<std::uint32_t>(std::alignment_of_v<T_TYPES>)... };
            const auto              Sizes                   = std::array{ static_cast<std::uint32_t>(sizeof(T_TYPES))... };

            for( int i=0; i< Aligments.size(); ++i )
            {
                Offsets[i]  = xcore::bits::Align( p, Aligments[i] );
                p           = Offsets[i] + Sizes[i];
            }

            // Make sure that p is ready to be aligned with the next full entry
            p = xcore::bits::Align(p, Aligments[0]);

            return std::tuple{ Offsets, p };
        }();
        constexpr static auto           size_of_one_v       = std::get<1>(offset_tuple_v);
        template<typename T>
        constexpr static auto           offset_of_type_v    = std::get<0>(offset_tuple_v)[xcore::types::tuple_t2i_v<T,std::tuple<T_TYPES...>>];

        struct iterator
        {
            self&           m_This;
            std::uint32_t   m_I;

            inline      auto operator   ++  ( void )                    noexcept
            {
                m_I++;
                return *this;
            }
            constexpr   bool operator   !=  (std::nullptr_t)    const   noexcept { return m_This.m_Count != m_I; }
            constexpr   auto operator   *   ( void )            const   noexcept
            {
                auto pA = &m_This.m_Pointer[m_I * size_of_one_v];
                return std::tuple<const T_TYPES& ...>
                {
                    reinterpret_cast<const T_TYPES&>(pA[offset_of_type_v<T_TYPES>])...
                };
            }
            constexpr   auto operator   *   ( void )                    noexcept
            {
                auto pA = &m_This.m_Pointer[m_I * size_of_one_v];
                return std::tuple<T_TYPES& ...>
                {
                    reinterpret_cast<T_TYPES&>(pA[offset_of_type_v<T_TYPES>])...
                };
            }
        };

    public:

        constexpr   auto begin()                        const   noexcept { return iterator{ *this, 0 }; }
                    auto begin()                                noexcept { return iterator{ *this, 0 }; }
        constexpr   auto end()                          const   noexcept { return nullptr;              }
        constexpr   auto size()                         const   noexcept { return m_Count;              }

        pool_v4()
        {
            if (s_PageSize == 0)
            {
                SYSTEM_INFO sSysInfo;
                GetSystemInfo(&sSysInfo);
                s_PageSize = sSysInfo.dwPageSize;
                s_PageSizePow2 = xcore::bits::Log2Int(s_PageSize);
                xassert((1u<< s_PageSizePow2) == s_PageSize );
            }

            std::size_t PageCount = (max_entries_v * size_of_one_v) / s_PageSize;
            PageCount = ((PageCount * s_PageSize) < (max_entries_v * size_of_one_v)) ? PageCount + 1 : PageCount;
            xassert((PageCount * s_PageSize) >= (max_entries_v * size_of_one_v));

            m_Pointer = reinterpret_cast<std::byte*>(VirtualAlloc(nullptr, PageCount * s_PageSize, MEM_RESERVE, PAGE_NOACCESS));
        }


        ~pool_v4(void) noexcept
        {
            if constexpr (((std::is_destructible_v<T_TYPES>) || ...) )
            {
                clear();
            }

            if (m_Pointer) VirtualFree(m_Pointer, 0, MEM_RELEASE);
        }

        template< typename...T_ARGS >
        std::size_t append(T_ARGS&&... Args) noexcept
        {
            static_assert(sizeof...(Args) == 0 || sizeof...(Args) == sizeof...(T_TYPES));

            // We want to detect when we cross over from block to block so we could define it as
            // ((m_Count+1) * size_of_one_v) / s_PageSize != (m_Count * size_of_one_v) / s_PageSize
            //
            // Because we know that s_PageSize is a power of two we can turn it into shifts
            // ((m_Count+1) * size_of_one_v) >> s_PageSizePow2 != (m_Count * size_of_one_v) >> s_PageSizePow2
            //
            // We can rewrite the above expression as 
            // (m_Count * size_of_one_v + size_of_one_v) >> s_PageSizePow2 != (m_Count * size_of_one_v) >> s_PageSizePow2
            //
            // We need to compute the ByteOffset not matter what and that is
            // ByteOffset = m_Count * size_of_one_v;
            //
            // So we can rewrite the original expression in terms of the ByteOffset
            // (ByteOffset + size_of_one_v) >> s_PageSizePow2 != ByteOffset >> s_PageSizePow2

            const auto ByteOffset = m_Count * size_of_one_v;
            const auto k          = (ByteOffset + size_of_one_v) >> s_PageSizePow2;
            if( m_Count == 0 || k != (ByteOffset >> s_PageSizePow2) )
            {
                const auto n = m_Count == 0 ? 0 : k;
                      auto p = VirtualAlloc(&m_Pointer[n * s_PageSize], s_PageSize, MEM_COMMIT, PAGE_READWRITE);
                xassert(p);
            }

            // construct the entries
            if constexpr ( sizeof...(T_ARGS) > 0 )
            {
                (
                    (new(&m_Pointer[ByteOffset + offset_of_type_v<T_TYPES>]) T_TYPES{ std::forward<T_ARGS>(Args) })
                ,   ...
                );
            }
            else if constexpr (((std::is_constructible_v<T_TYPES>) || ...))
            {
                (
                    (new(&m_Pointer[ByteOffset + offset_of_type_v<T_TYPES>]) T_TYPES)
                ,   ...
                );
            }

            return ++m_Count;
        }

        bool deleteBySwap(std::size_t Index) noexcept
        {
            bool Ret = false;
            xassert(Index < m_Count);
            --m_Count;

            const auto k = m_Count * size_of_one_v;
            if (Index != m_Count)
            {
                auto pA = &m_Pointer[Index * size_of_one_v];
                auto pB = &m_Pointer[k];
                (
                    (             reinterpret_cast<T_TYPES&>(pA[offset_of_type_v<T_TYPES>])
                      = std::move(reinterpret_cast<T_TYPES&>(pB[offset_of_type_v<T_TYPES>])))
                    , ...
                );
                Ret = true;
            }
            else if constexpr (((std::is_destructible_v<T_TYPES>) || ...))
            {
                (
                    (std::destroy_at(&reinterpret_cast<T_TYPES&>(m_Pointer[k + offset_of_type_v<T_TYPES>])))
                    , ...
                );
            }

            const auto kk = (k + size_of_one_v) >> s_PageSizePow2;
            if (m_Count == 0 || kk != (k >> s_PageSizePow2))
            {
                auto n = m_Count == 0 ? 0 : kk;
                auto b = VirtualFree(&m_Pointer[n * s_PageSize], s_PageSize, MEM_DECOMMIT);
                xassert(b);
            }

            return Ret;
        }

        template< typename T_TYPE, typename T_INDEX >
        auto& operator[](T_INDEX Index) noexcept
        {
            xassert(Index<m_Count);
            return reinterpret_cast<T_TYPES&>(m_Pointer[Index * size_of_one_v + offset_of_type_v<T_TYPES>]);
        }

        template< typename T_TYPE, typename T_INDEX >
        constexpr auto& operator[](T_INDEX Index) const noexcept
        {
            xassert(Index < m_Count);
            return reinterpret_cast<const T_TYPES&>(m_Pointer[Index * size_of_one_v + offset_of_type_v<T_TYPES>]);
        }

        void clear(void) noexcept
        {
            if constexpr (((std::is_destructible_v<T_TYPES>) || ...)) for (std::uint32_t i = 0; i < m_Count; ++i)
            {
                (
                    (std::destroy_at(&reinterpret_cast<T_TYPES&>(m_Pointer[i * size_of_one_v + offset_of_type_v<T_TYPES>])))
                    , ...
                );
            }

            auto nPages = (m_Count * size_of_one_v) / s_PageSize;
            nPages = (nPages* s_PageSize) != (m_Count * size_of_one_v) ? nPages+1 : nPages;
            VirtualFree(m_Pointer, nPages*s_PageSize, MEM_DECOMMIT);
            m_Count = 0;
        }

    protected:

        std::byte*                      m_Pointer;
        std::size_t                     m_Count{ 0 };
        static inline int               s_PageSize{ 0 };
        static inline int               s_PageSizePow2{ 0 };
    };
}