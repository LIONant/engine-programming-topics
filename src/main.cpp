#include "xcore.h"

#include <windows.h>
#include <memoryapi.h>

#include "engprog_linear_containers_test.h"
#include "engprog_quantum_containers_test.h"
#include "engprog_cache_test.h"
#include "engprog_events_test.h"
#include "engprog_general_types_tuples_test.h"
#include "engprog_multi-threaded_examples.h"
#include "engprog_scheduler_examples.h"
#include <cstdlib>
#include <new>

//------------------------------------------------------------------------------------
// main
//------------------------------------------------------------------------------------
int main(void)
{
    xcore::Init("Engine Programming");

    if(0) engprog::multi_threaded::examples::Test();
    if(0) engprog::cache::examples::Test();
    if(0) engprog::event::examples::Test();
    if(0) engprog::linear_containers::examples::Test(); 
    if(0) engprog::quantum_containers::examples::Test();
    if(0) engprog::general_types::tuples::examples::Test();
    if(1) engprog::scheduler::examples::Test();
    xcore::Kill();
    return 0;
}