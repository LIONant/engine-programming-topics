
#include "engprog_general_types_tuples.h"

namespace engprog::general_types::tuples::examples
{
    //--------------------------------------------------------------------------------
    // Test
    //--------------------------------------------------------------------------------
    void Test() noexcept
    {
        // V1
        tuple_v1<int, float, const char*> MyTuple;
        MyTuple.get<const char*>() = "Hello World";
        MyTuple.get<int>        () = 22;
        MyTuple.get<float>      () = 0.5f;

        // V2
        tuple_v2<float, int, int, const int*, int, int, const char*> MyTupleV2;

        tuple_v2<> Empty;

        Empty.count();

        MyTupleV2.get<float>        () = 0.5f;
        MyTupleV2.get<int, 0>       () = 20;
        MyTupleV2.get<int, 1>       () = 21;
        MyTupleV2.get<int, 2>       () = 22;
        MyTupleV2.get<int, 3>       () = 23;
        MyTupleV2.get<float>        () = 266;
        MyTupleV2.get<const char*>  () = "Hello World";
        MyTupleV2.get<const int*>   () = nullptr;


        MyTupleV2.get<0>() = 0.0f;
        MyTupleV2.get<1>() = 1;
        MyTupleV2.get<2>() = 2;
        MyTupleV2.get<3>() = nullptr;
        MyTupleV2.get<4>() = 4;
        MyTupleV2.get<5>() = 5;
        MyTupleV2.get<6>() = "3";
    }
}