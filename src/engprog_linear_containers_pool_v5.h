

namespace engprog::linear_containers
{
    // VM Multi-type packed types per page.
    template< std::size_t T_MAX_ENTRIES_V, typename...T_TYPES >
    class pool_v5
    {
    public:

        using                           self                = pool_v5;
        constexpr static auto           max_entries_v       = T_MAX_ENTRIES_V;
        constexpr static auto           page_size_v         = 4096u; // size of one page
        constexpr static auto           page_size_pow_v     = xcore::bits::Log2Int(page_size_v);
        constexpr static auto           offset_tuple_v      = []() constexpr
        {
            std::array<std::uint32_t, sizeof...(T_TYPES)>   Offsets{};

            const auto      Aligments               = std::array{ static_cast<std::uint32_t>(std::alignment_of_v<T_TYPES>)... };
            const auto      Sizes                   = std::array{ static_cast<std::uint32_t>(sizeof(T_TYPES))...              };
                  auto      HowManyNodesPerPage     = static_cast<std::uint32_t>(page_size_v / ((sizeof(T_TYPES)) + ...));

            for( ;; )
            {
                std::uint32_t p = 0;

                for( int i = 0; i < Aligments.size(); ++i )
                {
                    Offsets[i]  = xcore::bits::Align(p, Aligments[i]);
                    p           = Offsets[i] + Sizes[i] * HowManyNodesPerPage;
                }

                if( p <= page_size_v ) break;

                --HowManyNodesPerPage;
                xassert(HowManyNodesPerPage);
            }

            return std::tuple{ Offsets, HowManyNodesPerPage };
        }();

        constexpr static auto           entries_per_page_v  = std::get<1>(offset_tuple_v);
        template<typename T>
        constexpr static auto           offset_of_type_v    = std::get<0>(offset_tuple_v)[xcore::types::tuple_t2i_v<T,std::tuple<T_TYPES...>>];
        constexpr static auto           max_pages           = max_entries_v > ((max_entries_v / entries_per_page_v)*entries_per_page_v)? (max_entries_v / entries_per_page_v)+1 : (max_entries_v / entries_per_page_v);
        struct iterator
        {
            self&           m_This;
            std::uint32_t   m_ITotal;       // Total entries we went through
            std::uint32_t   m_I;            // Current entry in page
            std::uint32_t   m_Page;         // Which page are we in

            inline      auto operator   ++  ( void )                    noexcept
            {
                m_ITotal++;
                m_I++;
                if( m_I == entries_per_page_v)
                {
                    m_I = 0;
                    m_Page++;

                    auto pPage = &m_This.m_Pointer[m_Page * page_size_v];
                    ((_mm_prefetch((const char*)&pPage[offset_of_type_v<T_TYPES>], _MM_HINT_NTA)), ... );
                }
                return *this;
            }
            constexpr   bool operator   !=  (std::nullptr_t)    const   noexcept { return m_This.m_Count != m_ITotal; }
            constexpr   auto operator   *   ( void )            const   noexcept
            {
                auto pPage = &m_This.m_Pointer[m_Page * page_size_v];
                return std::tuple<const T_TYPES& ...>
                {
                    reinterpret_cast<const T_TYPES&>(pPage[offset_of_type_v<T_TYPES> + sizeof(T_TYPES)*m_I] )...
                };
            }
            constexpr   auto operator   *   ( void )                    noexcept
            {
                auto pPage = &m_This.m_Pointer[m_Page * page_size_v];
                return std::tuple<T_TYPES& ...>
                {
                    reinterpret_cast<T_TYPES&>(pPage[offset_of_type_v<T_TYPES> +sizeof(T_TYPES) * m_I])...
                };
            }
        };

    public:

        constexpr   auto begin()                        const   noexcept { return iterator{ *this, 0, 0, 0 }; }
                    auto begin()                                noexcept { return iterator{ *this, 0, 0, 0 }; }
        constexpr   auto end()                          const   noexcept { return nullptr;              }
        constexpr   auto size()                         const   noexcept { return m_Count;              }

        pool_v5()
        {
            if (s_PageSize == 0)
            {
                SYSTEM_INFO sSysInfo;
                GetSystemInfo(&sSysInfo);
                s_PageSize = sSysInfo.dwPageSize;
                xassert(page_size_v <= sSysInfo.dwPageSize);
            }

            m_Pointer = reinterpret_cast<std::byte*>(VirtualAlloc(nullptr, max_pages * page_size_v, MEM_RESERVE, PAGE_NOACCESS));
        }


        ~pool_v5(void) noexcept
        {
            if constexpr (((std::is_destructible_v<T_TYPES>) || ...) )
            {
                clear();
            }

            if (m_Pointer) VirtualFree(m_Pointer, 0, MEM_RELEASE);
        }

        template< typename...T_ARGS >
        std::size_t append(T_ARGS&&... Args) noexcept
        {
            static_assert(sizeof...(Args) == 0 || sizeof...(Args) == sizeof...(T_TYPES));

            std::byte*      pPage;
            std::uint32_t   Index;
            if( m_Count%entries_per_page_v == 0 )
            {
                pPage = reinterpret_cast<std::byte*>(VirtualAlloc(&m_Pointer[(m_Count/entries_per_page_v)*page_size_v], page_size_v, MEM_COMMIT, PAGE_READWRITE));
                Index = 0;
                xassert(pPage);
            }
            else
            {
                pPage = &m_Pointer[(m_Count / entries_per_page_v) * page_size_v];
                Index = m_Count%entries_per_page_v;
            }

            // construct the entries
            if constexpr ( sizeof...(T_ARGS) > 0 )
            {
                (
                    (new(&pPage[offset_of_type_v<T_TYPES> + Index * sizeof(T_TYPES)]) T_TYPES{ std::forward<T_ARGS>(Args) })
                ,   ...
                );
            }
            else if constexpr (((std::is_constructible_v<T_TYPES>) || ...))
            {
                (
                    (new(&pPage[offset_of_type_v<T_TYPES> + Index * sizeof(T_TYPES)]) T_TYPES)
                ,   ...
                );
            }

            return ++m_Count;
        }

        bool deleteBySwap(std::size_t Index) noexcept
        {
            bool Ret = false;
            xassert(Index < m_Count);
            --m_Count;

            if (Index != m_Count)
            {
                      auto* pPageA = &m_Pointer[(Index   / entries_per_page_v) * page_size_v];
                      auto* pPageB = &m_Pointer[(m_Count / entries_per_page_v) * page_size_v];
                const auto  IA     = (Index   % entries_per_page_v);
                const auto  IB     = (m_Count % entries_per_page_v);
                (
                    (             reinterpret_cast<T_TYPES&>(pPageA[offset_of_type_v<T_TYPES> + IA * sizeof(T_TYPES)])
                      = std::move(reinterpret_cast<T_TYPES&>(pPageB[offset_of_type_v<T_TYPES> + IB * sizeof(T_TYPES)])))
                    , ...
                );
                Ret = true;
            }
            else if constexpr (((std::is_destructible_v<T_TYPES>) || ...))
            {
                      auto* pPage = &m_Pointer[(Index / entries_per_page_v) * page_size_v];
                const auto  I     = (Index % entries_per_page_v);
                (
                    (std::destroy_at(&reinterpret_cast<T_TYPES&>(pPage[offset_of_type_v<T_TYPES> + I * sizeof(T_TYPES)])))
                    , ...
                );
            }

            if( (m_Count%entries_per_page_v) == 0)
            {
                auto* pPage = &m_Pointer[(m_Count / entries_per_page_v) * page_size_v];
                auto  b     = VirtualFree(pPage, page_size_v, MEM_DECOMMIT);
                xassert(b);
            }

            return Ret;
        }

        template< typename T_TYPE, typename T_INDEX >
        auto& operator[](T_INDEX Index) noexcept
        {
            xassert(Index<m_Count);
                  auto* pPage = &m_Pointer[(Index / entries_per_page_v) * page_size_v];
            const auto  I     = (Index % entries_per_page_v);
            return reinterpret_cast<T_TYPE&>(pPage[offset_of_type_v<T_TYPE> + I * sizeof(T_TYPE)]);
        }

        template< typename T_TYPE, typename T_INDEX >
        constexpr auto& operator[](T_INDEX Index) const noexcept
        {
            xassert(Index < m_Count);
                  auto* pPage = &m_Pointer[(Index / entries_per_page_v) * page_size_v];
            const auto  I     = (Index % entries_per_page_v);
            return reinterpret_cast<const T_TYPE&>(pPage[offset_of_type_v<T_TYPE> + I * sizeof(T_TYPE)]);
        }

        void clear(void) noexcept
        {
            if constexpr (((std::is_destructible_v<T_TYPES>) || ...))
            {
                std::uint32_t iPage = 0;
                std::uint32_t i     = 0;

                for (std::uint32_t iTotal = 0; iTotal < m_Count; ++iTotal)
                {
                    auto* pPage = &m_Pointer[ iPage * page_size_v ];

                    (
                        (std::destroy_at( &reinterpret_cast<T_TYPES&>( pPage[offset_of_type_v<T_TYPES> + i * sizeof(T_TYPES) ]) ))
                        , ...
                    );

                    i++;
                    if(i == entries_per_page_v )
                    {
                        i = 0;
                        iPage++;
                    }
                }
            }

            auto nPages = ( m_Count / entries_per_page_v );
            VirtualFree(m_Pointer, nPages*page_size_v, MEM_DECOMMIT);
            m_Count = 0;
        }

    protected:

        std::byte*                      m_Pointer;
        std::size_t                     m_Count{ 0 };
        static inline int               s_PageSize{ 0 };
    };
}
