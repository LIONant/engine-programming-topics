namespace engprog::general_types::tuples
{
    namespace details
    {
        namespace v1
        {
            template< typename T, typename...T_ARGS >
            struct tuple_var;

            template< typename T >
            struct tuple_var<T>
            {
                template< typename T_TYPE >
                T_TYPE& get();

                template<>
                T& get<T>()
                {
                    return m_Var;
                }

                T m_Var;
            };

            template< typename T, typename...T_ARGS >
            struct tuple_var : tuple_var< T_ARGS... >
            {
                template< typename T_TYPE >
                T_TYPE& get()
                {
                    return tuple_var< T_ARGS... >::template get<T_TYPE>();
                }

                template<>
                T& get<T>() { return m_Var; }

                T m_Var;
            };

            template< typename...T_TYPES >
            struct tuple : tuple_var< T_TYPES... >
            {
                static constexpr auto count() noexcept { return sizeof...(T_TYPES); }
            };
        }

        ///////////////////////////////////////////////////////////////////

        namespace v2
        {
            //-------------------------------------------------------------------------------------------
            // Very simple argument list. contains a value of how big it is
            //-------------------------------------------------------------------------------------------
            template< typename...T_ARGS >
            struct arg_list
            {
                static constexpr std::uint16_t value = sizeof...(T_ARGS);
            };

            //-------------------------------------------------------------------------------------------
            // given a type and index it determines the relative index of that type for the given index.
            //-------------------------------------------------------------------------------------------
            template< typename T_KEY, std::uint16_t T_INDEX_V, typename T, typename...T_ARG_LIST >
            struct get_local_index_internal
            {
                static_assert(T_INDEX_V>=0);
                static constexpr std::uint16_t value = []() constexpr -> std::uint16_t
                {
                    if constexpr (T_INDEX_V > 1) return get_local_index_internal< T_KEY, T_INDEX_V-1, T_ARG_LIST... >::value + std::is_same_v<T_KEY, T>;
                    else                         return std::is_same_v<T_KEY, T>;
                }();
            };

            template< typename T_KEY, std::uint16_t T_INDEX_V, typename...T_ARG_LIST >
            constexpr std::uint16_t get_local_index( arg_list<T_ARG_LIST...>* )
            {
                static_assert(T_INDEX_V > 0 );
                static_assert(T_INDEX_V <= arg_list<T_ARG_LIST...>::value );
                return get_local_index_internal< T_KEY, arg_list<T_ARG_LIST...>::value - T_INDEX_V + 1, T_ARG_LIST... >::value;
            }

            //-------------------------------------------------------------------------------------------
            // simple type to differenciate between same key types
            //-------------------------------------------------------------------------------------------
            template< typename T, std::uint16_t N, bool is_local >
            struct make_unique { T& m_Var; };

            //-------------------------------------------------------------------------------------------
            // this is the core internals of the tuple  
            //-------------------------------------------------------------------------------------------
            template< typename T_ARG_LIST, std::uint16_t T_INDEX_V, typename T, typename...T_ARGS >
            struct tuple_var : std::conditional_t< T_INDEX_V == 1, arg_list<>, tuple_var< T_ARG_LIST, T_INDEX_V - 1, T_ARGS..., void > >
            {
                using   local_unique_t  = make_unique< T, get_local_index< T, T_INDEX_V>(static_cast<T_ARG_LIST*>(nullptr)) - 1, true >;
                using   global_unique_t = make_unique< T, T_ARG_LIST::value - T_INDEX_V, false >;

                inline operator local_unique_t  () noexcept { return { m_Var }; }
                inline operator global_unique_t () noexcept { return { m_Var }; }

                T m_Var;
            };

            //-------------------------------------------------------------------------------------------
            // given a type list and an index we can return the type at the given index  
            //-------------------------------------------------------------------------------------------
            template< std::size_t I, typename T, typename...T_ARG_LIST >
            struct get_type_by_index
            {
                static_assert( sizeof...(T_ARG_LIST) > 0 );
                static_assert( I >= 1 );
                using type = typename get_type_by_index< I - 1, T_ARG_LIST...>::type;
            };

            template< typename T, typename...T_ARG_LIST >
            struct get_type_by_index< 0, T, T_ARG_LIST... >
            {
                using type = T;
            };

            //-------------------------------------------------------------------------------------------
            // the official tuple class
            //-------------------------------------------------------------------------------------------
            template< typename...T_TYPES >
            struct tuple : tuple_var< arg_list< T_TYPES...>, sizeof...(T_TYPES), T_TYPES... >
            {
                using parent_t = tuple_var< arg_list< T_TYPES...>, sizeof...(T_TYPES), T_TYPES... >;

                template< typename T, std::uint16_t N = 0>
                inline T& get() noexcept
                {
                    static_assert( N < getCountOf<T>() );
                    using requested_type = make_unique<T, N, true>;
                    return static_cast<requested_type>(*this).m_Var;
                }

                template< std::uint16_t I >
                inline auto& get() noexcept
                {
                    using key_type       = typename get_type_by_index<I, T_TYPES...>::type;
                    using requested_type = make_unique<key_type, I, false>;
                    return static_cast<requested_type>(*this).m_Var;
                }

                template< typename T >
                constexpr static auto getCountOf() noexcept
                {
                    return get_local_index<T, 1>((arg_list< T_TYPES...>*)nullptr);
                }

                constexpr auto count() const noexcept { return sizeof...(T_TYPES); }
            };

            template<>
            struct tuple<>
            {
                template< typename T >
                constexpr static auto getCountOf() noexcept
                {
                    return 0;
                }

                constexpr auto count() const noexcept { return 0; }
            };
        }
    }

    template< typename...T_TYPES >
    using tuple_v1 = details::v1::tuple< T_TYPES... >;

    template< typename...T_TYPES >
    using tuple_v2 = details::v2::tuple< T_TYPES... >;
}
