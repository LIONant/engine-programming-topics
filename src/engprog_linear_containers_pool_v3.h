
namespace engprog::linear_containers
{
    // VM multi-type alloc per type
    template< std::size_t T_MAX_ENTRIES_V, typename...T_TYPES >
    class pool_v3
    {
    public:

        constexpr static std::size_t    max_entries_v       = T_MAX_ENTRIES_V;
        using                           self                = pool_v3;
        
        struct iterator
        {
            self&       m_This;
            std::size_t m_I;

            inline      auto operator   ++  ( void )                    noexcept { m_I++; return *this; }
            constexpr   bool operator   !=  (std::nullptr_t)    const   noexcept {return m_This.m_Count != m_I; }
            constexpr   auto operator   *   ( void )            const   noexcept { return std::tuple<const T_TYPES&...> { std::get<T_TYPES*>(m_This.m_Pointers)[m_I]... }; }
            constexpr   auto operator   *   ( void )                    noexcept { return std::tuple<T_TYPES&...>       { std::get<T_TYPES*>(m_This.m_Pointers)[m_I]... }; }
        };

    public:

        constexpr   auto begin()                        const   noexcept { return iterator{ *this, 0 };    }
                    auto begin()                                noexcept { return iterator{ *this, 0 };    }
        constexpr   auto end()                          const   noexcept { return nullptr;                }
        constexpr   auto size()                         const   noexcept { return m_Count; }

        pool_v3()
        {
            if(s_PageSize==0)
            {
                SYSTEM_INFO sSysInfo;
                GetSystemInfo(&sSysInfo);
                s_PageSize      = sSysInfo.dwPageSize;
                s_PageSizePow2  = xcore::bits::Log2Int(s_PageSize);
                xassert((1u << s_PageSizePow2) == s_PageSize);
            }

            auto Init = [&]( auto& P ) constexpr
            {
                using t = xcore::types::decay_full_t<decltype(P)>;
                std::size_t PageCount = (max_entries_v * sizeof(t)) / s_PageSize;
                            PageCount = ((PageCount * s_PageSize) < (max_entries_v * sizeof(t))) ? PageCount + 1 : PageCount;
                xassert((PageCount * s_PageSize) >= (max_entries_v * sizeof(t)));

                P = reinterpret_cast<t*>(VirtualAlloc(nullptr, PageCount * s_PageSize, MEM_RESERVE, PAGE_NOACCESS));
            };

            ( (Init(std::get<T_TYPES*>(m_Pointers))), ... );
        }

        ~pool_v3(void) noexcept
        {
            clear();

            auto FreePerType = [&](auto& P) constexpr
            {
                using t = xcore::types::decay_full_t<decltype(P)>;
                if (P) VirtualFree(P, 0, MEM_RELEASE);
                P = nullptr;
            };

            ((FreePerType(std::get<T_TYPES*>(m_Pointers))), ...);
        }

        template< typename...T_ARGS >
        std::size_t append(T_ARGS&&... Args) noexcept
        {
            auto AppendPerType = [&](auto& P, auto&& Arg ) constexpr
            {
                using t = xcore::types::decay_full_t<decltype(P)>;

                // We want to detect when we cross over from block to block so we could define it as
                // ((m_Count+1) * size_of_one_v) / s_PageSize != (m_Count * size_of_one_v) / s_PageSize
                //
                // Because we know that s_PageSize is a power of two we can turn it into shifts
                // ((m_Count+1) * size_of_one_v) >> s_PageSizePow2 != (m_Count * size_of_one_v) >> s_PageSizePow2
                //
                // We can rewrite the above expression as 
                // (m_Count * size_of_one_v + size_of_one_v) >> s_PageSizePow2 != (m_Count * size_of_one_v) >> s_PageSizePow2
                //
                // We need to compute the ByteOffset not matter what and that is
                // ByteOffset = m_Count * size_of_one_v;
                //
                // So we can rewrite the original expression in terms of the ByteOffset
                // (ByteOffset + size_of_one_v) >> s_PageSizePow2 != ByteOffset >> s_PageSizePow2

                const auto ByteOffset = m_Count * sizeof(t);
                const auto k          = (ByteOffset + sizeof(t)) >> s_PageSizePow2;
                if( m_Count == 0 || k != (ByteOffset >> s_PageSizePow2) )
                {
                    auto pRaw  = &reinterpret_cast<std::byte*>(P)[k * s_PageSize];
                    auto p     = VirtualAlloc(pRaw, s_PageSize, MEM_COMMIT, PAGE_READWRITE);
                    xassert(p == pRaw);
                }

                // construct the entry
                if constexpr (std::is_constructible_v<t> )
                {
                    if constexpr ( std::is_same_v<std::decay_t<decltype(Arg)>, void> )
                    {
                        new(&P[m_Count]) t;
                    }
                    else
                    {
                        new(&P[m_Count]) t{ Arg };
                    }
                }
            };

            static_assert( sizeof...(Args) == 0 
                        || sizeof...(Args) == sizeof...(T_TYPES) );

            ((AppendPerType(std::get<T_TYPES*>(m_Pointers), Args )), ... );

            return ++m_Count;
        }

        void deleteBySwap(std::size_t Index) noexcept
        {
            xassert(Index < m_Count);
            --m_Count;
            if (Index != m_Count)
            {
                auto SwapDeletePerType = [&](auto& P) constexpr
                {
                    using t = xcore::types::decay_full_t<decltype(P)>;

                    P[Index] = P[m_Count];

                    const auto ByteOffset = m_Count * sizeof(t);
                    const auto k = (ByteOffset + sizeof(t)) >> s_PageSizePow2;
                    if (m_Count == 0 || k != (ByteOffset >> s_PageSizePow2))
                    {
                        auto pRaw = &reinterpret_cast<std::byte*>(P)[k * s_PageSize];
                        auto b    = VirtualFree( pRaw, s_PageSize, MEM_DECOMMIT);
                        xassert(b);
                    }
                };

                ((SwapDeletePerType(std::get<T_TYPES*>(m_Pointers))), ...);
            }
            else
            {
                auto DeletePerType = [&](auto& P) constexpr
                {
                    using t = xcore::types::decay_full_t<decltype(P)>;
                    if constexpr (std::is_destructible_v<t>)
                    {
                        std::destroy_at(&P[Index]);
                    }

                    const auto ByteOffset = m_Count * sizeof(t);
                    const auto k = (ByteOffset + sizeof(t)) >> s_PageSizePow2;
                    if (m_Count == 0 || k != (ByteOffset >> s_PageSizePow2))
                    {
                        auto pRaw = &reinterpret_cast<std::byte*>(P)[k * s_PageSize];
                        auto b    = VirtualFree(pRaw, s_PageSize, MEM_DECOMMIT);
                        xassert(b);
                    }
                };

                ((DeletePerType(std::get<T_TYPES*>(m_Pointers))), ...);
            }
        }

        template< typename T_TYPE, typename T_INDEX >
        auto& operator[](T_INDEX Index) noexcept
        {
            xassert(Index<m_Count);
            return std::get<T_TYPE*>()[Index];
        }

        template< typename T_TYPE, typename T_INDEX >
        constexpr auto& operator[](T_INDEX Index) const noexcept
        {
            xassert(Index < m_Count);
            return std::get<T_TYPE*>()[Index];
        }

        void clear(void) noexcept
        {
            auto ClearPerType = [&](auto& P) constexpr
            {
                using t = xcore::types::decay_full_t<decltype(P)>;
                if constexpr (std::is_destructible_v<t>)
                {
                    for (int i = 0; i < m_Count; ++i)
                        std::destroy_at(&P[i]);
                }
            };

            ((ClearPerType(std::get<T_TYPES*>(m_Pointers))), ...);

            m_Count = 0;
        }

    protected:

        std::tuple<T_TYPES* ... >       m_Pointers;
        std::size_t                     m_Count{ 0 };
        static inline int               s_PageSize{ 0 };
        static inline int               s_PageSizePow2{ 0 };
    };
}