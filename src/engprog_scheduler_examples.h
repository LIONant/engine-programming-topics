namespace engprog::scheduler::examples
{
    //--------------------------------------------------------------------

    template< typename T_PARENT = xcore::scheduler::trigger<1> >
    struct start_trigger final : T_PARENT
    {
        start_trigger() : T_PARENT{ xcore::scheduler::lifetime::DONT_DELETE_WHEN_DONE, xcore::scheduler::triggers::DONT_CLEAN_COUNT }{}
    };

    //--------------------------------------------------------------------

    template< typename T_PARENT = xcore::scheduler::trigger<1> >
    struct end_trigger final : T_PARENT
    {
        end_trigger() : T_PARENT{ xcore::scheduler::lifetime::DONT_DELETE_WHEN_DONE, xcore::scheduler::triggers::DONT_CLEAN_COUNT }{}
        virtual void qt_onTriggered(void) noexcept override
        {
            // Indicate this is the end of the frame
            XCORE_PERF_FRAME_MARK_END("Frame")

            printf("\nEndTrigger->%d\n",Loops);
            if (--Loops)
            {
                // Put all our dependencies into the quantum world
                T_PARENT::qt_onTriggered();
            }
            else
            {
                // Tell the scheduler that we want our main thread back!
                xcore::get().m_Scheduler.MainThreadStopWorking();

                // Make sure we still reset the notification counter
                T_PARENT::m_NotificationCounter.store(T_PARENT::m_NotificationResetCounter, std::memory_order_relaxed);
            }

            // After the above line this part could be in the quantum world
        }

        int Loops = 100;
    };

    //--------------------------------------------------------------------

    template< typename T_PARENT = xcore::scheduler::job<1> >
    struct world final : public T_PARENT
    {
                        world   ( void ) noexcept : T_PARENT( xcore::scheduler::definition::Flags(xcore::scheduler::triggers::DONT_CLEAN_COUNT) ){}
        virtual void    qt_onRun( void ) noexcept override
        {
            // Start of the frame
            XCORE_PERF_FRAME_MARK_START("Frame")
        }
    };

    //--------------------------------------------------------------------

    template< typename T_PARENT = xcore::scheduler::job<1> >
    struct physics final : public T_PARENT
    {
        physics(void) noexcept : T_PARENT( xcore::scheduler::definition::Flags(xcore::scheduler::triggers::DONT_CLEAN_COUNT) )
        {
            T_PARENT::setupName(xconst_universal_str("Physics"));
            m_Particles.New( 5000000 ).CheckError();
        }

        virtual void qt_onRun(void) noexcept override
        {
            // Do some work here if we have to...
            xcore::scheduler::channel Channel(xconst_universal_str("Physics::Foreach"));
            if(0) Channel.ForeachFlat(m_Particles, 10000, [&]( xcore::span<xcore::vector3> Range )
            {
                 for( auto& e : Range )
                 {
                     e += xcore::vector3(1.0f,0.0f,0.0f);
                 }
            });
            else Channel.ForeachLog( m_Particles, 10, 10000, [&](xcore::span<xcore::vector3> Range)
            {
                for (auto& e : Range)
                {
                    e += xcore::vector3(1.0f, 0.0f, 0.0f);
                }
            });


            Channel.join();
        }

        xcore::unique_span< xcore::vector3 > m_Particles;
    };

    //--------------------------------------------------------------------
    //    +-------+               +---------+
    //    | World |--->(Start)--->| Physics |--->(End)---->"Loop to World"
    //    +-------+               +---------+
    //--------------------------------------------------------------------
    void Test()
    {
        world           World;
        physics         Physics;
        start_trigger   StartTrigger;
        end_trigger     EndTrigger;

        StartTrigger.JobWillNotifyMe(World);
        StartTrigger.AddJobToBeTrigger(Physics);

        EndTrigger.JobWillNotifyMe(Physics);
        EndTrigger.AddJobToBeTrigger(World);

        // Add the graph into the quantum world
   //     xcore::get().m_Scheduler.m_LogChannel->TurnOn();
        xcore::get().m_Scheduler.AddJobToQuantumWorld(World);
        xcore::get().m_Scheduler.MainThreadStartsWorking();

        int a=0;
    }
}