
#include "engprog_events.h"

namespace engprog::event::examples
{
    struct system
    {
        struct events
        {
            using mytest1 = event::type< void(int) >;
            using mytest2 = event::type< void() >;
            using mytest3 = event::type< void(event::handle) >;

            mytest1 m_Test1;
            mytest2 m_Test2;
            mytest3 m_Test3;
        };

        events m_Events;
    };

    using global_mytest1 = event::type< void(int) >;
    global_mytest1          g_MyTest1{};
    system::events::mytest3 g_MyTest2{};

    void Test1(int) {}
    void Test2(event::handle){}

    struct test
    {
        void OnTest1(int)  {}
        void OnTest2(void) {}
        test(system& System)
        {
            m_MyTestDelegate1.setup(System.m_Events.m_Test1);
            m_MyTestDelegate2.setup(System.m_Events.m_Test2);
            m_MyTestDelegate3.setup(g_MyTest1);

            g_MyTest1.append< Test1 > ();
            g_MyTest1.append< +[](int){} >(5);
            g_MyTest2.append< Test2 > ((void*)this);
        }

        ~test()
        {
            g_MyTest1.remove< Test1 >();
            g_MyTest1.remove(5);
            g_MyTest2.remove(this);
        }

        system::events::mytest1::delegate<&test::OnTest1> m_MyTestDelegate1{ this };
        system::events::mytest2::delegate<&test::OnTest2> m_MyTestDelegate2{ this };
        global_mytest1::delegate<Test1>                   m_MyTestDelegate3{};
    };

    //--------------------------------------------------------------------------------
    // Test
    //--------------------------------------------------------------------------------
    void Test() noexcept
    {
        printf("#################################################################################\n");
        printf("# Testing the event system\n");
        printf("#################################################################################\n\n");

        {
            system A;
            test   B(A);

            g_MyTest1.NotifyAll(33);
            A.m_Events.m_Test1.NotifyAll(99);
            g_MyTest2.NotifyAll({});
        }

        int a = 0;
    }
}
