### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

### [Asset Pipeline]()

# Source Control

* Just like source code it is useful to have a history for assets
  * Build a particular version
  * Get back an older version of the asset
  * Etc
* There are two types of source controls
  * Centralize source controls
    * SVN, Perforce, etc...
    * Top Pro: Only 1 version of data in users computer
    * Top Con: Everything depends on the server
  * Decentralize source controls
    * Git, Mercurial, etc...
    * Top Pro: Everyone is independent from each other
    * Top Con: The entire history is in each client
  * If a project would use a decentralize source control for assets you will need a huge hard-drive for each user and when pushing and pulling it will take forever.
    * The reasons why Git also supports an extension for large files

# Advance features 

* Using centralize source control also give you other powers...
   * See what every one is working on
   * Exclusive locking a file
      * Binary files don't merge unlike source code
      * It prevents two people from editing the same binary file
      * Promotes communication between the team
    * Allows to integrate your asset pipeline deeply with source control
      * By adding knowledge of the source control in your tools
         * Can give you a warning if two people are working on the same asset and whom... 

# Creating Label or Tags

* Every source control has a labeling / Tagging system
  * Allows to give a name to an specific version of your branch
  * This allows to quickly identify a particular version
* Every project have milestones
   * Tagging a milestone at submission time is a good idea
      * May need to debug it later
      * May need to rebuild it for a demo
      * Etc
* Ideally your assets and your code should use the same source control. 
   * This allows for easy tagging 
   * Simplifies the pipeline

# Branches

* Every source control has a way to create branches
* Branches are useful to minimize conflicts across groups in your team.
    * AI programmers may not care about Graphics programmers
    * Some people may be "fixing a bug" or "Working on a special deadline" while the rest of the team is moving forward.
* A good source control can will not duplicate assets across branches if it does not need to do so
    * Not need to worry about duplicating everything...
