### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

# Prefabs

* A tool used to create game objects with pre-specified values on its components
* Prefabs are also a resource type
   * However the files don't need to be binary
* Most game engines support this feature
   * [Unity Prefabs](https://docs.unity3d.com/Manual/Prefabs.html)
   * [Unreal Blueprints](https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Blueprints/GettingStarted/)

## Prefab Instance

* These are the actual instances of a prefab that are place in the actual scene
   * It is an instanciation of a prefab
   * These guys get serialize as part of the scene
* A core feature of this tool is the concept overriting properties
   * Makes [Properties](properties/properties.md) very important for a game engine
   * You can not really have a proper prefab system without properties
* A full feature prefab instance would have these abilities
   * Overrite properties
     * Also Know which properties have been overriten
   * Add or Delete components
     * Know which components have been added of deleted
 * A quick data structure could look as follows:

|**Members**|**Description**|
|:---:|:---|
| resource_handle | A handle to the prefab resource, this is typically a 64 bit GUID
| std::vector<component_type_state> | List of components that have been added or deleted
| std::vector<property_overrides> | List of properties overriten for a given component

## Prefav Instance in Editor

* Usually prefab Instances are mark in blue in the Hirarchy/Scene viewer

 <a href="./Documentation/prefabs/images/Image01.jpg"><img src="images/Image01.jpg" width="300"></a>

* Properties that are overriten shows in bold in the inspector panel
  * You can always delete the override

 <a href="./Documentation/prefabs/images/Image02.png"><img src="images/Image02.png" width="400"></a>

## Prefab Variant

* Also known as recursive prefabs
  * Because a change in a prefab affects all its Variants
* It is a type of prefab
  * It is a resource
  * You can create Prefab Instances from it
* It is somewhat similar to a Prefab Instance
  * Knows which prefab it comes from 
  * Have the same overrides than Prefab Instances 
    * Override properties
    * Override Components 
* You can create Prefab Instance from the Variants
  * This is how they end up in the scene
  * When in the editor a prefab or prefab Variant is change you will need to update all its descendants (All affected instances both direct and indirect)
* Other engines support it
  * [Unity Variants](https://docs.unity3d.com/Manual/PrefabVariants.html)


## Prefab Serialization 

* Typically prefabs are saved as txt even though they are resources
  * They still need to solve the future proof thing...
  * Can still be handy for debug
* Because they are resources they should have a GUID as well
  * This is how prefab instances refer to them
  * This is how Variants also refer to other Variants 

  