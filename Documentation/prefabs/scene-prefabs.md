### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

# Scene Prefabs
* Scene prefabs are actually like regular scenes
  * Can have Entities, Prefab-instances, and Scene-Prefab-Instances
* They always have a root entity
  * All the scene entities/instances are its descendent of this entity
  * Root entity is used for placement
  * Root entity can go away after instantiation if user desires
    * But will break the link between the Prefab and the instance
* They are resources
  * Similar to a regular prefab

