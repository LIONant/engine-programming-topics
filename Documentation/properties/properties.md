### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

# Properties


* **Properties** are basically a **reflection system**
* **Reflection** is a language ability to generalize access to:
  * Member Variables
  * Member Functions
* **Generalize Access** means that there is a universal **data driven** way to access objects
  * C# has a reflection system but C++ does not
* **Data Driven** means that compilation is unnecessary to access an object or modify its behavior

## Why to have a property system?

* They factor out how to access objects
  * Because they are a **reflection system**
  * So we can…
* Display/Edit objects in editors
* Serialize/Deserialize Objects to disk
* Copy/Paste Objects
* Allow for proper prefabs
* Help organize the code
* Etc… Etc… The user cases is long

## Proof that they are useful!

* All game engines use property systems
  * Which means that they are a core concept

|**Unreal**|**Unity**|**xCore**|
|:---:|:---:|:---:|
| <a href="./Documentation/properties/images/Image01.png"><img src="images/Image01.png" width="500"></a> | <a href="./Documentation/properties/images/Image02.png"><img src="images/Image02.png" width="500"></a> | <a href="./Documentation/properties/images/Image03.png"><img src="images/Image03.png" width="500"></a> |

## C++ Property Systems

* No supported natively in the language
* Bunch of libraries out there:
  * RTTR - https://github.com/rttrorg/rttr 
    * More friendly to use
    * More mature
  * LIONant Properties - https://gitlab.com/LIONant/properties
    * Faster
    * ImGUI Example

## Property Panels (Inspectors)

<a href="./Documentation/properties/images/Image04.png"><img src="images/Image04.png" width="1000"></a>

## Serializing Objects

* Properties are a perfect way to serialize all objects
  * Not need to write code per component
* The requirement for a good serialization:
  * Save in text
     * Good for debugging
  * Future Proof
     * Since game objects will change as you iterate in your game development…
     * You need to be able to load old data
  * Entities references are handle well
     * Certain ECS need to be able to remap entities ID at serialization time

## Prefabs

* You can not write a proper prefab system without a property system
* The core concept of a prefab system is the override of properties

| <a href="./Documentation/properties/images/Image05.png"><img src="images/Image05.png" width="800"></a> |
|:---|
| **UNITY EXAMPLE:**<br> Note the bold letters inside the list of properties. This means that the user has overwritten that property with a new value |

* The user can always delete the override to get back the old value from the original prefab
* To be able to do any of this you will need a property system

## Conclusion

* Property system is a core concept for game engines
* Property systems save allot of work
   * One serialization to rule them all
   * One way to display objects!
   * Etc
* Allows you to do new things
   * Prefabs
   * Visual Scripting
   * Etc
* It is a must have!
