### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

# Entity IDs

All Game Engines have some entity-id
* This id is how one entity can have a reference to another entity
  * Such is the case with an enemy AI to the player
* There ids are definitely no pointers
  * Must serialize them
  * Must ask the engine if the entity is alive
* Engine can return pointers from an entity-id (if still alive)
  * So these pointers could be null if the entity is dead
  * Pointers should never be store in any component or system
    * ECS move entities/components in memory all the time
    * Entity could die at any time 

## Entity-IDs as GUIDs (Global Unique IDs)
* Old method (Used in game object systems) 
* These IDs are the most convenient to use
  * They are tricky to generate right
  * They must guarantee minimum possible collision
    * A collision could be a disaster
* Usually this GUIDs are a look up to a map with the object pointer
* GUIDs are used for other systems like resource pipelines
   * Typically 128bits UID
      * 64bits for just for instance
      * 64bits for type
* You can look at [xcore how it generates GUIDs](https://gitlab.com/LIONant/xcore/-/blob/master/src/xcore_guid.h?ref_type=heads)

## Entity-IDs (new ECS) as Special Indices 

<a href="./Documentation/entity-id/images/ECS-ID.png"><img src="images/ECS-ID.png" width="1000"></a> <br> Here is a typical 64bit ID for an ECS 

* New method used in ECS
* These IDs are truly unique but comes with overhead
  * Can not hide any entity references from the property system
  * If you do it will create a bug
* You must be able to remap any Entity IDs at load time
  * Entity themselves 
  * Any reference to entities
  * Can be done easily because of the property system

## Global Entity-IDs (new ECS)

* Example of global entities
  * The player
  * UI Elements (HUD)
* These IDs look the same as regular IDs except the "isGlobal" bit will be on
* These are problematic because it must solve
  * IDs can not change because 
    * Reference to this ID can be in any Scene
    * Even unloaded scenes
  * Multiple users
    * Must survive tooling like Git, etc...
    * Deletion of the global Entity
  * The ID can not collide with any other ID
* When loading Global-IDs must be remap to a local ID 
  * isGlobal flag must still be on
  * These local IDs must be remap to the Original globalIDs 
    * When serialize them out
      * Includes Entities and References
  * 2 way Hash map between the GlobalID and LocalID may be needed   
* Treat these Global-IDs like a resource GUID
  * Function to map between EntityID and ResourceID may be required
* Entity becomes like a resource
  * Visible in the resource Tooling
    * Easy to assign 
    * Easy to see all global entities
  * Resource GUID collision tooling
  * Global Entity serialized data 
    * Method 1: Entity Resource is Similar to a Scene-Blueprint
      * But they can't be instantiated again
      * Life time is ambiguous. 
        * Is up to the user to instantiate and kill
    * Method 2: Entity Resource is just a link
      * Contains information to which scene the global entity is  
      * Life time for the Entity is associated with the Scene
