### [Engine Programming Topics](../main.md) <img src="https://i.imgur.com/TyjrCTS.jpg" align="right" width="220px" /><br>

# Input System

* Input is a system because it helps the communication between the hardware and the scripts.
* Because of this, all engines have an Input System
   * Unity, Unreal, etc…
* The benefits is that we can change the inputs of the game without caring about the scripts.

## Type of input hardware

* Mouse, trackpad
  * X,Y (Delta) – Float or Integer, units:  pixels
* Buttons
  * X (Key Code) – integer, units: keycode
* Touchpad
  * X,Y (Position) – Float or Integer, units: pixels
* Mouse Wheel
  * X (Delta) – Float or Integer, units: pixels
* Pad or Joystick
  * X,Y (Position) – Float or Integer, units: parametric 

## Additional hardware issues

* Events:
  * Connect / Disconnect
* Rumble
  * How to rumble the hardware
* Device ID
  * When handling multiple input devices each device will have its own ID
* Dead Zones
  * Hardware are not 100% and sometimes the input is unstable
  * There are regions you can set to ignore the input
  * For Pads could be near the (X,Y)(0,0)

## What scripts should know

* Scripts should not know anything about hardware
  * They only deal with Logical things
* Example of Logical Inputs
  * Input Object
    * Vector2D – Movement
      * Forward, Left, Right, Backwards
    * Int getID()
      * -1 is disconnected
* As you can see there is not concept of hardware
  * Is it a mouse? Is it a keyboard?
  * Scripts should not know and not care

## Core Features

* The input system should be able to map Logical to Physical
* Such “Movement” is a Normalize 2D vector
  * Connected to Keyboard
  * Connected to Device-Joystick1
  * With DeadZone at (0.1,0.1)
* This mapping will allow the user to ask the input system about “movement” and get back the relevant vector.
* Another mapping could be “Jump” or “Shot” or “Menu:Up”, etc

# References

* Unity Input system:
  * https://docs.unity3d.com/Packages/com.unity.inputsystem@1.7/manual/index.html
* Unreal Input system:
  * 5.x - https://docs.unrealengine.com/5.2/en-US/input-in-unreal-engine/
  * 4.x - https://docs.unrealengine.com/4.26/en-US/InteractiveExperiences/Input/
