# Engine Programming Topics


Here is some useful notes respect to Virtual Memory Cross Platform for: Android/iOS/OSX/Unix/...
https://android.developreference.com/article/18941184/C%2b%2b+porting+VirtualFree+in+OS+X
Some Notes:
C++ porting VirtualFree in OS X - c++
I'm trying to make a port of our memory management in which some allocators use virtual memory mechanism to reserve address space without (at the beginning) allocate any physical memory and later allocate memory only when they need.
The code is based on Windows' VirtualAlloc and VirtualFree to make the things work, now I'm trying to port this code to Apple OS X, that as far as I know, doesn't have such API, after a while I come up with the following code:
//to reserve virtual address space
//equivalent of VirtualAlloc(NULL, size, MEM_RESERVE, PAGE_NOACCESS)
void* ptr = mmap(NULL, size, PROT_NONE, (MAP_PRIVATE | MAP_ANON), -1, 0);
msync(ptr, size, (MS_SYNC | MS_INVALIDATE));

//to free ALL virtual address space
//equivalent of VirtualFree(addr, 0, MEM_RELEASE)
//where "size" is the size of the entire virtual address space and "addr" the starting address
msync(addr, size, MS_SYNC);
munmap(addr, size);

//to allocate physical memory
//equivalent of VirtualAlloc(addr, size, MEM_COMMIT, PAGE_READWRITE)
void* ptr = mmap(addr, size, (PROT_READ | PROT_WRITE), (MAP_FIXED | MAP_SHARED | MAP_ANON), -1, 0);
msync(addr, size, (MS_SYNC | MS_INVALIDATE));

The only thing I can't figure out is how to port the use of VirtualFree to free/uncommit only a part of the physical memory, mimic the VirtualFree call:
VirtualFree(addr, size, MEM_DECOMMIT);

I have tried to call munmap with the desired address and size but it doesn't release the memory... while calling it to free all the virtual space works perfectly.
Can someone help me in this task?

I found the answer of my own question thanks to this blog:
http://blog.nervus.org/managing-virtual-address-spaces-with-mmap/
I also post here his solution in case the link dies:
void DecommitMemory(void* addr, size_t size)
{
// instead of unmapping the address, we're just gonna trick
// the TLB to mark this as a new mapped area which, due to
// demand paging, will not be committed until used.
mmap(addr, size, PROT_NONE, MAP_FIXED|MAP_PRIVATE|MAP_ANON, -1, 0);
msync(addr, size, MS_SYNC|MS_INVALIDATE);
}

